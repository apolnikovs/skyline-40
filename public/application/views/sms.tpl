{extends "DemoLayout.tpl"}


    {block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $SMSPage}
    {/block}
    
    {block name=afterJqueryUI}
        
        <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script> 
        
        <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
   
        <style type="text/css" >

            .ui-combobox-input {
                 width:300px;
             }  
        </style>
    
    {/block}
 
    {block name=scripts}


    <script type="text/javascript">
        
    var $statuses = [
                    {foreach from=$statuses item=st}
                       ["{$st.Name}", "{$st.Code}"],
                    {/foreach}
                    ]; 
                    
            
        
    function gotoEditPage($sRow)
    {
        
         
        $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
        $('#updateButtonId').trigger('click');
       
       
    }

    function inactiveRow(nRow, aData)
    {
        
        if (aData[6]==$statuses[1][1])
        {  
            $(nRow).addClass("inactive");

            $('td:eq(5)', nRow).html( $statuses[1][0] );
        }
        else
        {
            $(nRow).addClass("");
                  $('td:eq(5)', nRow).html( $statuses[0][0] );
        }
    }
    
  

    $(document).ready(function() {


                

                  //Click handler for finish button.
                  $(document).on('click', '#finish_btn', 
                                function() {

                                
                                     $(location).attr('href', '{$_subdomain}/SystemAdmin/index/lookupTables');

                                });







                   /* =======================================================
                    *
                    * set tab on return for input elements with form submit on auto-submit class...
                    *
                    * ======================================================= */

                    $('input[type=text],input[type=password]').keypress( function( e ) {
                            if (e.which == 13) {
                                $(this).blur();
                                if ($(this).hasClass('auto-submit')) {
                                    $('.auto-hint').each(function() {
                                        $this = $(this);
                                        if ($this.val() == $this.attr('title')) {
                                            $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                            if ($this.hasClass('auto-pwd')) {
                                                $this.prop('type','password');
                                            }
                                        }
                                    } );
                                    $(this).get(0).form.onsubmit();
                                } else {
                                    $next = $(this).attr('tabIndex') + 1;
                                    $('[tabIndex="'+$next+'"]').focus();
                                }
                                return false;
                            }
                        } );        



                     

                   var  displayButtons = "UA";
                     
                     
                     


                    
                    $('#smsResults').PCCSDataTable( {

			"aoColumns": [ 
			    /* SMSID   */  { 'bVisible': false },    
			    /* SMSName	    */  null,
                            /* Brand    */  null,
                            /* BrandID    */  null,
                            /* Created	    */  null,
                            /* Created by    */  null,
                            /* Status    */  null
			],
                            
                            "aaSorting": [[ 1, "asc" ]],
                            
                            
                            
                               
                            displayButtons:  displayButtons,
                            addButtonId:     'addButtonId',
                            addButtonText:   '{$page['Buttons']['insert']|escape:'html'}',
                            createFormTitle: '{$page['Text']['insert_page_legend']|escape:'html'}',
                            createAppUrl:    '{$_subdomain}/LookupTables/sms/insert/',
                            createDataUrl:   '{$_subdomain}/LookupTables/ProcessData/SMS/',
                            formInsertButton:'insert_save_btn',
                            
                            frmErrorRules:   {
                                                    BrandID:
                                                        {
                                                            required: true
                                                        },
                                                    SMSName:
                                                        {
                                                            required: true
                                                        }, 
                                                    SMSBodyText:
                                                        {
                                                            required: true
                                                        }, 
                                                    Status:
                                                        {
                                                            required: true
                                                        }     
                                             },
                                                
                           frmErrorMessages: {
                                                
                                                   
                                                    BrandID:
                                                        {
                                                            required: "{$page['Errors']['brand_error']|escape:'html'}"
                                                        },
                                                    SMSName:
                                                        {
                                                            required: "{$page['Errors']['name_error']|escape:'html'}"
                                                        }, 
                                                    SMSBodyText:
                                                        {
                                                            required: "{$page['Errors']['body_error']|escape:'html'}"
                                                        }, 
                                                    Status:
                                                        {
                                                            required: "{$page['Errors']['status_error']|escape:'html'}"
                                                        }    
                                              },                     
                            
                            popUpFormWidth:  815,
                            popUpFormHeight: 330,
                            
                            
                            updateButtonId:  'updateButtonId',
                            updateButtonText:'{$page['Buttons']['edit']|escape:'html'}',
                            updateFormTitle: '{$page['Text']['update_page_legend']|escape:'html'}',
                            updateAppUrl:    '{$_subdomain}/LookupTables/sms/update/',
                            updateDataUrl:   '{$_subdomain}/LookupTables/ProcessData/SMS/',
                            formUpdateButton:'update_save_btn',
                            
                            colorboxFormId:  "smsForm",
                            frmErrorMsgClass:"fieldError",
                            frmErrorElement: "label",
                            htmlTablePageId: 'smsResultsPanel',
                            htmlTableId:     'smsResults',
                            fetchDataUrl:    '{$_subdomain}/LookupTables/ProcessData/SMS/fetch/',
                            formCancelButton:'cancel_btn',
                           // pickCallbackMethod: 'openJob',
                            dblclickCallbackMethod: 'gotoEditPage',
                            fnRowCallback:          'inactiveRow',
                            searchCloseImage:'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
                            tooltipTitle:    "{$page['Text']['tooltip_title']|escape:'html'}",
                            iDisplayLength:  25,
                            formDataErrorMsgId: "suggestText",
                            frmErrorSugMsgClass:"formCommonError"


                        });
                      



                   

    });

    </script>

    {/block}

    {block name=body}

    <div class="breadcrumb">
        <div>

            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/lookupTables" >{$page['Text']['lookup_tables']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}

        </div>
    </div>



    <div class="main" id="home" >

               <div class="LTTopPanel" >
                    <form id="smsForm1" name="smsForm1" method="post"  action="#" class="inline">

                        <fieldset>
                        <legend title="" >{$page['Text']['legend']|escape:'html'}</legend>
                        <p>
                            <label>{$page['Text']['description']|escape:'html'}</label>
                        </p> 

                        </fieldset> 


                    </form>
                </div>  


                <div class="LTResultsPanel" id="smsResultsPanel" >

                    <table id="smsResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                        <thead>
                                <tr>
                                        <th width="20%" title="{$page['Text']['sms_id']|escape:'html'}" >{$page['Text']['sms_id']|escape:'html'}</th>
                                        <th title="{$page['Text']['sms_name']|escape:'html'}" >{$page['Text']['sms_name']|escape:'html'}</th>
                                        <th title="{$page['Text']['brand']|escape:'html'}" >{$page['Text']['brand']|escape:'html'}</th>
                                        <th title="{$page['Text']['brand_id']|escape:'html'}" >{$page['Text']['brand_id']|escape:'html'}</th>
                                        <th title="{$page['Text']['created']|escape:'html'}" >{$page['Text']['created']|escape:'html'}</th>
                                        <th title="{$page['Text']['created_by']|escape:'html'}" >{$page['Text']['created_by']|escape:'html'}</th>
                                        <th width="10%" title="{$page['Text']['status']|escape:'html'}" >{$page['Text']['status']|escape:'html'}</th>
                                </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>  
                                
                </div>        

                <div class="bottomButtonsPanel" >
                    <hr>
                    
                    <button id="finish_btn" type="button" class="gplus-blue rightBtn" ><span class="label">{$page['Buttons']['finish']|escape:'html'}</span></button>
                </div>        


    </div>
                        
                        



{/block}



