{if $accessErrorFlag eq true} 
    
    <div id="accessDeniedMsg" class="SystemAdminFormPanel"  >   
    
        <form id="accessDeniedMsgForm" name="accessDeniedMsgForm" method="post"  action="#" class="inline">
                <fieldset>
                    <legend title="" > {$page['Text']['error_page_legend']|escape:'html'} </legend>
                    <p>

                        <label class="formCommonError" >{$accessDeniedMsg|escape:'html'}</label><br><br>

                    </p>

                    <p>

                        <span class= "bottomButtons" >


                            <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                        </span>

                    </p>


                </fieldset>   
        </form>            


    </div>    
    
{else}  
     <link rel="Stylesheet" type="text/css" href="{$_subdomain}/css/themes/pccs/jPicker-1.1.6.css" />
     <script src="{$_subdomain}/js/jpicker-1.1.6.js" type="text/javascript"></script>    
    <script type="text/javascript" >	
	
         $.fn.jPicker.defaults.images.clientPath = "{$_subdomain}/css/themes/pccs/images/colorPicker/";
var defDiaryType="{$datarow.DiaryType}";

        function addFields(selected) {	
                    var selected_split = selected.split(","); 	


                    $("#BuildingNameNumber").val(selected_split[0]).blur();	
                    $("#Street").val(selected_split[1]).blur();
                    $("#LocalArea").val(selected_split[2]).blur();
                    $("#TownCity").val(selected_split[3]).blur();
                    var spf = $("#SPFormFieldset").width();
                    $("#selectOutput").slideUp("slow"); 
                    $("#SPFormFieldset").width(spf);

                }
                
    var oAuthTable;
    var savedHeight=null;
    {if $datarow.ServiceProviderID eq '' || $datarow.ServiceProviderID eq '0'}
    var AuthManufacturersSavePending = false;
    {/if}
       
    function PostAuthMan( service_provider_key ) { 
        {if $datarow.ServiceProviderID eq '' || $datarow.ServiceProviderID eq '0'}
        {* Service Provider Insert mode, so only do update if a Save is Pending *}
        if (!AuthManufacturersSavePending) return;
        {/if}       
        var post_data = $('input',oAuthTable.fnGetNodes()).serialize();       
        if (post_data == '') 
            post_data = 'asc='+service_provider_key;
        else 
            post_data += '&asc='+service_provider_key;
        $.ajax({
                type: "POST",
                url: '{$_subdomain}'+"/OrganisationSetup/UpdateManufacturerAuth",
                data: post_data,
                success: function(msg){
                                    // save updated auth and checked values                                   
                                    $('.auth',oAuthTable.fnGetNodes()).each(
                                              function() {
                                                  var id = $(this).val();
                                                  var data = oAuthTable.fnGetData();
                                                  for (var i=0; i<data.length; i++) {
                                                      if (data[i][3] == id) {
                                                          if ($(this).is(':checked')) {
                                                              oAuthTable.fnUpdate('1', i, 4, false);
                                                          } else {
                                                              oAuthTable.fnUpdate('0', i, 4, false);
                                                          }
                                                          break;
                                                       }
                                                  }
                                              }
                                         );
                                    $('.check',oAuthTable.fnGetNodes()).each(
                                              function() {
                                                  var id = $(this).val();
                                                  var data = oAuthTable.fnGetData();
                                                  for (var i=0; i<data.length; i++) {
                                                      if (data[i][4] == id) {
                                                          if ($(this).is(':checked')) {
                                                              oAuthTable.fnUpdate('1', i, 5, false);
                                                          } else {
                                                              oAuthTable.fnUpdate('0', i, 5, false);
                                                          }
                                                          break;
                                                       }
                                                  }
                                              }
                                         ); 
                         }
           });
    }
       
    function setCountryBasedOnCounty($countyId, $countryId, $parentCountryId)
    {
        $($countyId+" option:selected").each(function () {
            $selected_cc_id =  $($countyId+" option:selected").attr('id');
            $country_id_array = $selected_cc_id.split('_');
            if($country_id_array[1]!='0')
            {
                $($countryId).val($country_id_array[1]);
                setValue($countryId);
            }
            else
            {
                 $($parentCountryId+" input:first").val('');
            }
        });
    }   
    $(document).ready(function() {
    
    
    $('#DocColour1').jPicker( {
          window:
          {
            expandable: true,
			position:
			{
			  x: 'screenCenter', // acceptable values "left", "center", "right", "screenCenter", or relative px value
			  y: '500px' // acceptable values "top", "bottom", "center", or relative px value
			}
          }
        }
     
    );
    $('#DocColour2').jPicker( {
          window:
          {
            expandable: true,
			position:
			{
			  x: 'screenCenter', // acceptable values "left", "center", "right", "screenCenter", or relative px value
			  y: '500px' // acceptable values "top", "bottom", "center", or relative px value
			}
          }
        }
     
    );
    $('#DocColour3').jPicker( {
          window:
          {
            expandable: true,
			position:
			{
			  x: 'screenCenter', // acceptable values "left", "center", "right", "screenCenter", or relative px value
			  y: '500px' // acceptable values "top", "bottom", "center", or relative px value
			}
          }
        }
     
    );
    
    $(".Color").css({ width: "25px", height: "24px", padding: "0px" });
    
    
    
    
        $("#CountyID").combobox({
            change: function() {
                setCountryBasedOnCounty("#CountyID", "#CountryID", "#P_CountryID");
            }
        });
        $("#CountryID").combobox({
            change: function() {
                var options = '';
		
		{* Updated By Praveen Kumar [start] 
		   If Republic Of Ireland, New Zealand, South Africa is selected then the Postcode field should NOT be compulsory
		*}		
		if($("#CountryID").val()!='1') {		   		   
		    $("#PostCodeSup").hide();
		    		    	    
		} else {		   	    
		    $("#PostCodeSup").show();		   
		    
		}
		{* Updated By Praveen Kumar [end] *}
		
                var selectedCountryRates = vat_rate_array[$('#CountryID').val()];
                $('#VATRateID').empty()
                $('#VATRateID').append('<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>');
                for (var n = 0; n < selectedCountryRates.length; n++) {
                    if (selectedCountryRates[n] !== undefined) {
                    options += '<option value="' + n + '">' + selectedCountryRates[n] + '</option>';
                    }
                }
                $('#VATRateID').append(options);
            }
        });
        $("#Platform").combobox({
            change: function() {
                if($("#Platform").val()=='ServiceBase')  
                {
                    $("#IPAddressSup").show();
                    $("#PortSup").show();
                    $(".fieldError[for='IPAddress']").fadeIn(); 
                    $(".fieldError[for='Port']").fadeIn(); 
                }
                else
                {
                    $("#IPAddressSup").hide();
                    $("#PortSup").hide();
                    $(".fieldError[for='IPAddress']").fadeOut(); 
                    $(".fieldError[for='Port']").fadeOut(); 
                }
            }
        });
        $('#VATRateID').combobox();
        $("#ClientID").combobox();
             function autoHint()
             {
                 $('.auto-hint').each(function() {
                                    $this = $(this);
                                    if ($this.val() == $this.attr('title')) {
                                        $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                        if ($this.hasClass('auto-pwd')) {
                                            $this.prop('type','password');
                                        }
                                    }
                        } );  
             }
             
             /*$(document).on('change', '#Platform', 
                                function() {
                                
                                if($("#Platform").val()=='ServiceBase')  
                                {
                                    $("#IPAddressSup").show();
                                    $("#PortSup").show();
                                    $(".fieldError[for='IPAddress']").fadeIn(); 
                                    $(".fieldError[for='Port']").fadeIn(); 
                                }
                                else
                                {
                                    $("#IPAddressSup").hide();
                                    $("#PortSup").hide();
                                    
                                    $(".fieldError[for='IPAddress']").fadeOut(); 
                                    $(".fieldError[for='Port']").fadeOut(); 
                                }
                                
                
               });*/
             
             {if $datarow.ServiceProviderID neq '' && $datarow.ServiceProviderID neq '0' && $nClients|@count gt 0} 
         
             {foreach $clientContacts as $cc}
             
                $("#ClientID option[value='{$cc.ClientID}']").hide();
                
             {/foreach}
             
             {/if}
             
             $(document).on('click', '#insert_save_btn', 
                                function() {

                                autoHint();
                
               });
               
             $(document).on('click', '#update_save_btn', 
                                function() {
                                
                                autoHint();
                
               }); 
               
             oAuthTable = $('#AuthorisedManufacturersTable').dataTable( {
                       "aoColumns": [
                            null,
			    { sWidth: '72px', bSortable: true, 
                              mRender: function ( data, type, row ) {
                                       if (type === 'filter' || type === 'sort' || type === 'type') {
                                           return data;
                                       }                             
                                       if (data == 1)
                                           return '<input type="checkbox" name="auth[]" value="'+row[3]+'" class="auth-tag" checked />';
                                       else
                                           return '<input type="checkbox" name="auth[]" value="'+row[3]+'" class="auth-tag"/>';
                                   } 
                            },
                            { sWidth: '72px', bSortable: true,
                              mRender: function ( data, type, row ) {
                                       if (type === 'filter' || type === 'sort' || type === 'type') {
                                           return data;
                                       }                             
                                       if (data == 1)
                                           return '<input type="checkbox" name="check[]" value="'+row[3]+'" class="check-tag" checked />';
                                       else 
                                           return '<input type="checkbox" name="check[]" value="'+row[3]+'" class="check-tag" />';
                                   }                        
                            },
                            { bVisible: false },
                            { bVisible: false },
                            { bVisible: false }
		        ],
                        "bAutoWidth": false,
                        "bInfo": false,
                        "bLengthChange": true,
                        "iDisplayLength": 10,
                        "bPaginate": true,
                        "sPaginationType": "full_numbers",
                        "sDom": "ftlp"
                    } );
                    
            $(document).on('change', '#AuthorisedManufacturersTable_length select', 
                                function() {
                                    var h = $('#AuthoriseManufacturersFormPanel').height()+135;
                                    $('#colorbox').colorbox.resize({ height: h });               
                            });
               
             $(document).on('click', '#AuthManButton', 
                                function() {
                                    if (savedHeight == null) savedHeight = $('#cboxWrapper').height();
                                    console.log('savedHeight = '+savedHeight);
                                    $('#ServiceProvidersFormPanel').hide();
                                    var h = $('#AuthoriseManufacturersFormPanel').show().height()+135;
                                    $('#colorbox').colorbox.resize({ height: h });               
                            }); 
                
              $(document).on('click', '#authman_save_btn', 
                                function() { 
                                    {if $datarow.ServiceProviderID neq '' && $datarow.ServiceProviderID neq '0'}
                                    {* Service Provider Update mode.... *}
                                    PostAuthMan( {$datarow.ServiceProviderID} ); 
                                    {else}
                                    {* Mark as save pending if Service Provider Insert mode.... *}
                                    AuthManufacturersSavePending = true;
                                    {/if}
                                    $('#AuthoriseManufacturersFormPanel').hide();
                                    $('#ServiceProvidersFormPanel').show();
                                    $('#colorbox').colorbox.resize({ height: savedHeight }); 
                                    return false;
                            });    
                            
              $(document).on('click', '#authman_cancel_btn', 
                                function() { 
                                   {if $datarow.ServiceProviderID eq '' || $datarow.ServiceProviderID eq '0'}
                                   {* Cancel Save Pending if Service Provider Insert mode... *}
                                   AuthManufacturersSavePending = false;
                                   {/if}
                                    // reset auth and checked values
                                    var data = oAuthTable.fnGetData();
                                    for (var i=0; i<data.length; i++) {
                                        var orig_auth =  data[i][4];
                                        var orig_check = data[i][5];
                                        oAuthTable.fnUpdate(orig_auth, i, 1, false);
                                        oAuthTable.fnUpdate(orig_check, i, 2, false);
                                    }
                                    $('#AuthoriseManufacturersFormPanel').hide();
                                    $('#ServiceProvidersFormPanel').show();
                                    $('#colorbox').colorbox.resize({ height: savedHeight });
                                    return false;
                            });
                            
              $(document).on('click', '#authman_show_tagged', 
                                function() {                                   
                                    if ($(this).is(':checked')) { 
                                        oAuthTable.fnFilter('1',1);
                                    } else {
                                        oAuthTable.fnFilter('',1);
                                    }
                            });
                            
              $(document).on('click', '#checkman_show_tagged', 
                                function() {                                   
                                    if ($(this).is(':checked')) { 
                                        oAuthTable.fnFilter('1',2);
                                    } else {
                                       oAuthTable.fnFilter('',2);
                                    }
                            });
                            
              $(document).on('click', '.auth-tag',                                
                                function() {  
                                    var id = $(this).val();
                                    var data = oAuthTable.fnGetData();
                                    for (var i=0; i<data.length; i++) {
                                        if (data[i][3] == id) {
                                            if ($(this).is(':checked')) {
                                                oAuthTable.fnUpdate('1', i, 1, false);
                                            } else {
                                                oAuthTable.fnUpdate('0', i, 1, false);
                                            }
                                            break;
                                        }
                                    }
                            });
                            
             $(document).on('click', '.check-tag',                                
                                function() {  
                                    var id = $(this).val();
                                    var data = oAuthTable.fnGetData();
                                    for (var i=0; i<data.length; i++) {
                                        if (data[i][3] == id) {
                                            if ($(this).is(':checked')) {
                                                oAuthTable.fnUpdate('1', i, 2, false);
                                            } else {
                                                oAuthTable.fnUpdate('0', i, 2, false);
                                            }
                                            break;
                                        }
                                    }
                            });
                                          
            /* =======================================================
            *
            * Initialise input auto-hint functions...
            *
            * ======================================================= */

            $('.auto-hint').focus(function() {
                    $this = $(this);
                    if ($this.val() == $this.attr('title')) {
                        $this.val('').removeClass('auto-hint');
                        if ($this.hasClass('auto-pwd')) {
                            $this.prop('type','password');
                        }
                    }
                } ).blur(function() {
                    $this = $(this);
                    if ($this.val() == '' && $this.attr('title') != '')  {
                        $this.val($this.attr('title')).addClass('auto-hint');
                        if ($this.hasClass('auto-pwd')) {
                            $this.prop('type','text');
                        }
                    }         
                } ).each(function(){
                    $this = $(this);
                    if ($this.attr('title') == '') { return; }
                    if ($this.val() == '') { 
                        if ($this.attr('type') == 'password') {
                            $this.addClass('auto-pwd').prop('type','text');
                        }
                        $this.val($this.attr('title')); 
                    } else { 
                        $this.removeClass('auto-hint'); 
                    }
                    $this.attr('autocomplete','off');
                } ); 
                
                
                $("#Platform").trigger("change");
                
         });       
        
        
        function checkDiaryType(){
      
        if($('#diaryActiveID').attr("checked")=="checked"){
            $('#diaryTypeP').show();
            }else{
            $('#diaryTypeP').hide();
            }
        
        }
        
        function verify(y){
        var tt=prompt('The online diary defaults may be lost and the Service Center diary may go offline if this is changed in error. Please confirm if you wish to make this changes by typing yes in field below.','Type: yes');
        if(tt=='yes'||tt=='Yes'){
        $(y).attr("checked","checked");
        }else{
       
        $('#'+defDiaryType).attr('checked','checked');
        $(y).attr("checked",false);
        }
        }
	       
    </script>   
    
    
    <div id="ServiceProvidersFormPanel" class="SystemAdminFormPanel" >
    
                <form enctype="multipart/form-data" id="ServiceProvidersForm" name="ServiceProvidersForm" method="post"  action="#" class="inline">
       
                <fieldset id="SPFormFieldset">
                    <legend title="" >{$form_legend|escape:'html'}</legend>
                        
                    <p><label id="suggestText" ></label></p>
                            
                            <p>
                                <label ></label>
                                <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>
                            </p>
                       
                            
                           
                            
			<p>
			    <label class="fieldLabel" for="CompanyName">
				{$page['Labels']['company_name']|escape:'html'}:<sup>*</sup>
			    </label>
			    &nbsp;&nbsp; 
			    <input type="text" class="text" name="CompanyName" value="{$datarow.CompanyName|escape:'html'}" id="CompanyName" />
			</p>
                         
			<p>
			    <label class="fieldLabel" for="Acronym">
				{$page['Labels']['acronym']|escape:'html'}:<sup>*</sup>
			    </label>
			    &nbsp;&nbsp;
			    <input type="text" class="text" name="Acronym" value="{$datarow.Acronym|escape:'html'}" id="Acronym" />
			</p>
                        <p>
                            <label class="fieldLabel" for="CurrencyID" >Default Currency:</label>
                            &nbsp;&nbsp; 


                            <select name="CurrencyID" id="CurrencyID" class="text" >
                                <option value="" {if $datarow.CurrencyID|default:'' eq ''}selected="selected"{/if}>Select Currency</option>
                                {foreach $currcencyList|default:'' as $s}
                                    
                                    <option value="{$s.CurrencyID|default:''}"   {if $datarow.CurrencyID|default:'' eq $s.CurrencyID|default:''}selected="selected"{/if}>{$s.CurrencyCode|default:''}</option>
                                    
                                {/foreach}

                            </select>

                        </p>
                         
                        <p>			    
                            <label class="fieldLabel" for="PostalCode">{$page['Labels']['postcode']|escape:'html'}:<span id="PostCodeSup"><sup>*</sup></span></label>
                            &nbsp;&nbsp; <input  class="text uppercaseText" style="width: 140px;" type="text" name="PostalCode" id="PostalCode" value="{$datarow.PostalCode|escape:'html'}" >&nbsp;

                            <input type="submit" name="quick_find_btn" id="quick_find_btn" class="textSubmitButton postCodeLookUpBtn"  onclick="return false;"   value="Click to find Address" >

                        </p>

                        <p id="selectOutput" ></p>


                        <p>
                            <label class="fieldLabel" for="BuildingNameNumber" >{$page['Labels']['building_name']|escape:'html'}:</label>
                            &nbsp;&nbsp; <input type="text" class="text"  name="BuildingNameNumber" value="{$datarow.BuildingNameNumber|escape:'html'}" id="BuildingNameNumber" >          
                        </p>

                        <p>
                            <label class="fieldLabel" for="Street" >{$page['Labels']['street']|escape:'html'}:<sup>*</sup></label>
                            &nbsp;&nbsp; <input type="text" class="text"  name="Street" value="{$datarow.Street|escape:'html'}" id="Street" >          
                        </p>

                        <p>
                            <label class="fieldLabel" for="LocalArea" >{$page['Labels']['area']|escape:'html'}:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="LocalArea" value="{$datarow.LocalArea|escape:'html'}" id="LocalArea" >          
                        </p>


                        <p>
                            <label class="fieldLabel" for="TownCity" >{$page['Labels']['city']|escape:'html'}:<sup>*</sup></label>
                            &nbsp;&nbsp; <input type="text" class="text" name="TownCity" value="{$datarow.TownCity|escape:'html'}" id="TownCity" >          
                        </p>

                        <p>
                            <label class="fieldLabel" for="CountyID" >{$page['Labels']['county']|escape:'html'}:</label>
                            &nbsp;&nbsp; 


                            <select name="CountyID" id="CountyID" class="text" >
                                <option value="" id="country_0_county_0" {if $datarow.CountyID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>
                                {foreach $countries as $country}
                                    {foreach $country.Counties as $county}
                                    <option value="{$county.CountyID}"  id="country_{$country.CountryID}_county_{$county.CountyID}" {if $datarow.CountyID eq $county.CountyID}selected="selected"{/if}>{$county.Name|escape:'html'}</option>
                                    {/foreach}
                                {/foreach}

                            </select>

                        </p>
                         
                        
                        
                         <p id="P_CountryID">
                            <label class="fieldLabel" for="CountryID" >{$page['Labels']['country']|escape:'html'}:<sup>*</sup></label>
                            &nbsp;&nbsp; 
			    <select name="CountryID" id="CountryID" class="text">
                                <option value="" {if $datarow.CountryID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                
                                {foreach $countries as $country}

                                    <option value="{$country.CountryID}" {if $datarow.CountryID eq $country.CountryID}selected="selected"{/if}>{$country.Name|escape:'html'}</option>

                                {/foreach}
                                
                            </select>
                        </p>
                        
                         
                        <p>
                                <label class="fieldLabel" for="ContactEmail">{$page['Labels']['email']|escape:'html'}:<sup>*</sup></label>
                                &nbsp;&nbsp; <input  type="text" name="ContactEmail" class="text auto-hint" title="{$page['Text']['email']|escape:'html'}" value="{$datarow.ContactEmail|escape:'html'}" id="ContactEmail" >
                                <br>
                        </p>
                        
                        
                        
                        <p>
                                <label class="fieldLabel" for="ReplyEmail">{$page['Labels']['reply_email']|escape:'html'}:</label>
                                &nbsp;&nbsp; <input  type="text" name="ReplyEmail" class="text auto-hint"  value="{$datarow.ReplyEmail|escape:'html'}" id="ReplyEmail" >
                                <br>
                        </p>
                        
                        

                        <p>
                            <label class="fieldLabel" for="ContactPhone" >{$page['Labels']['phone']|escape:'html'}:<sup>*</sup></label>
                            &nbsp;&nbsp; <input  type="text" class="text phoneField"  name="ContactPhone" value="{$datarow.ContactPhone|escape:'html'}" id="ContactPhone" >
                            <input  type="text" class="text extField auto-hint" title="{$page['Text']['extension_no']|escape:'html'}" name="ContactPhoneExt" value="{$datarow.ContactPhoneExt|escape:'html'}" id="ContactPhoneExt" >

                        </p>
                        
                        
                        <p>
                                <label class="fieldLabel" for="PublicWebsiteAddress">{$page['Labels']['public_website_address']|escape:'html'}:</label>
                                &nbsp;&nbsp; <input  type="text" name="PublicWebsiteAddress" maxlength="100" class="text"  value="{$datarow.PublicWebsiteAddress|escape:'html'}" id="PublicWebsiteAddress" >
                                <br>
                        </p>
                        
                        
			<p>
			    <hr>
			</p>
                        
                        
                        <p>
			    <label class="fieldLabel" for="GeneralManagerForename">{$page['Labels']['general_manager']|escape:'html'}:</label>
			    &nbsp;&nbsp;
			    <input type="text" name="GeneralManagerForename" placeholder="Forename" style="width: 142px;" class="text" title="" value="{$datarow.GeneralManagerForename|escape:'html'}" id="GeneralManagerForename">
			    <input type="text" name="GeneralManagerSurname" placeholder="Surname" style="width: 142px;" class="text" title="" value="{$datarow.GeneralManagerSurname|escape:'html'}" id="GeneralManagerSurname">
			    <br>
                        </p>
			<p>
			    <label class="fieldLabel" for="GeneralManagerEmail">{$page['Labels']['general_manager_email']|escape:'html'}:</label>
			    &nbsp;&nbsp;
			    <input type="text" name="GeneralManagerEmail" class="text" title="" value="{$datarow.GeneralManagerEmail|escape:'html'}" id="GeneralManagerEmail">
			    <br>
                        </p>
                        
                        

			<p>
			    <label class="fieldLabel" for="ServiceManagerForename">{$page['Labels']['service_manager']|escape:'html'}:</label>
			    &nbsp;&nbsp;
			    <input type="text" name="ServiceManagerForename" placeholder="Forename" style="width: 142px;" class="text" title="" value="{$datarow.ServiceManagerForename|escape:'html'}" id="ServiceManagerForename">
			    <input type="text" name="ServiceManagerSurname" placeholder="Surname" style="width: 142px;" class="text" title="" value="{$datarow.ServiceManagerSurname|escape:'html'}" id="ServiceManagerSurname">
			    <br>
                        </p>
			<p>
			    <label class="fieldLabel" for="ServiceManagerEmail">{$page['Labels']['service_manager_email']|escape:'html'}:</label>
			    &nbsp;&nbsp;
			    <input type="text" name="ServiceManagerEmail" class="text" title="" value="{$datarow.ServiceManagerEmail|escape:'html'}" id="ServiceManagerEmail">
			    <br>
                        </p>
			
			
			<p>
			    <label class="fieldLabel" for="AdminSupervisorForename">{$page['Labels']['admin_supervisor']|escape:'html'}:</label>
			    &nbsp;&nbsp;
			    <input type="text" name="AdminSupervisorForename" placeholder="Forename" style="width: 142px;" class="text" title="" value="{$datarow.AdminSupervisorForename|escape:'html'}" id="AdminSupervisorForename">
			    <input type="text" name="AdminSupervisorSurname" placeholder="Surname" style="width: 142px;" class="text" title="" value="{$datarow.AdminSupervisorSurname|escape:'html'}" id="AdminSupervisorSurname">
			    <br>
                        </p>
			
			<p>
			    <label class="fieldLabel" for="AdminSupervisorEmail">{$page['Labels']['admin_supervisor_email']|escape:'html'}:</label>
			    &nbsp;&nbsp;
			    <input type="text" name="AdminSupervisorEmail" class="text" title="" value="{$datarow.AdminSupervisorEmail|escape:'html'}" id="AdminSupervisorEmail">
			    <br>
                        </p>
			
			<p>
			    <label class="fieldLabel" for="SendASCReport">{$page['Labels']['send_reports']|escape:'html'}:</label>
			    &nbsp;&nbsp; 
			    <input type="radio" name="SendASCReport" value="Yes" {if $datarow.SendASCReport == "Yes"}checked="checked"{/if} /><span class="text">Yes</span> 
			    <input type="radio" name="SendASCReport" value="No" {if $datarow.SendASCReport == "No"}checked="checked"{/if} /><span class="text">No</span> 
			</p>
			
			
			<p>
                            <hr>
                            
                            <p>
                             <label class="fieldLabel" for="" >{$page['Labels']['document_color_scheme']|escape:'html'}:<sup>*</sup></label>
                             <input onclick="$('#docColours').hide();" {if $datarow.DocumentCustomColorScheme|default:'Skyline' eq 'Skyline'}checked=checked{/if} type="radio" name="DocumentCustomColorScheme" value="Skyline"><span>{$page['Labels']['skyline']|escape:'html'}</span>
                             <input onclick="$('#docColours').show();" {if $datarow.DocumentCustomColorScheme|default:'Skyline' eq 'Customised'}checked=checked{/if} type="radio" name="DocumentCustomColorScheme" value="Customised"><span>{$page['Labels']['customised']|escape:'html'}</span>
                            </p>
                            <span id="docColours" {if $datarow.DocumentCustomColorScheme eq 'Skyline'} style="display:none;" {/if}>
                                
                            <p>
                            <label class="fieldLabel" for="DocColour1" >{$page['Labels']['document_header_text_colour']|escape:'html'}:<sup>*</sup></label>
                            <input class="text" value="{$datarow.DocColour1|default:"25AAE1"}" type="text" name="DocColour1" value="" id="DocColour1">
                            </p>
                            <p>
                            <label class="fieldLabel" for="DocColour2" >{$page['Labels']['document_column']|escape:'html'}:<sup>*</sup></label>
                            <input class="text" value="{$datarow.DocColour2|default:"0F75BC"}" type="text" name="DocColour2" value="" id="DocColour2">
                            </p>
                            <p>
                            <label class="fieldLabel" for="DocColour3" >{$page['Labels']['document_column_text']|escape:'html'}:<sup>*</sup></label>
                            <input class="text" value="{$datarow.DocColour3|default:"0F75BC"}" type="text" name="DocColour3" value="" id="DocColour3">
                            </p>
                            </span>
                             {if $datarow.ServiceProviderID neq '' && $datarow.ServiceProviderID neq '0'}
                            <p>
                            <label class="fieldLabel" for="DocLogo" >{$page['Labels']['document_logo']|escape:'html'}:</label>
                            <input class="text" type="file" name="DocLogo" value="" id="DocLogo"> <a href="{$_subdomain}/OrganisationSetup/showSampleDoc/{$datarow.ServiceProviderID}/" target="_new">View Saved Template</a>
                            </p>
                            {/if}
			    <hr>
			</p>
                        

			
                         <p>
                            <label class="fieldLabel" for="Platform" >{$page['Labels']['platform']|escape:'html'}:<sup>*</sup></label>
                           
                              &nbsp;&nbsp; 
                              <select name="Platform" id="Platform" class="text" >
                                <option value="" {if $datarow.Platform eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $platforms as $platform}

                                    <option value="{$platform.Code}" {if $datarow.Platform eq $platform.Code}selected="selected"{/if}>{$platform.Name|escape:'html'}</option>

                                {/foreach}

                            </select>
                        
                         </p>
                         
                         
                         <p>
                            <label class="fieldLabel" for="IPAddress" >{$page['Labels']['ip_address']|escape:'html'}:<sup id="IPAddressSup" >*</sup></label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="IPAddress" value="{$datarow.IPAddress|escape:'html'}" id="IPAddress" >
                        
                         </p>
                         
                         <p>
                            <label class="fieldLabel" for="Port" >{$page['Labels']['port']|escape:'html'}:<sup id="PortSup"  >*</sup></label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="Port" value="{$datarow.Port|escape:'html'}" id="Port" >
                        
                         </p>
                         
                         
                         <p>
                            <hr>
                         </p>
                         
                         
			<p>
			    <label class="fieldLabel" for="InvoiceToCompanyName" >{$page['Labels']['invoice_company_name']|escape:'html'}:</label>
			    &nbsp;&nbsp; 
			    <input type="text" class="text" name="InvoiceToCompanyName" value="{$datarow.InvoiceToCompanyName|escape:'html'}" id="InvoiceToCompanyName" />
			</p>
                         
                         
                         <p>
                            <label class="fieldLabel" for="VATRateID" >{$page['Labels']['vat_code']|escape:'html'}</label>
                           
                             &nbsp;&nbsp; 
                             
                            <select name="VATRateID" id="VATRateID" >
                            <option value="" {if $datarow.VATRateID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>
                            {if $datarow.CountryID && isset($vatRates[$datarow.CountryID])}    
                            {foreach $vatRates[$datarow.CountryID] as $vatRate}

                                <option value="{$vatRate.VatRateID}" {if $datarow.VATRateID eq $vatRate.VatRateID}selected="selected"{/if}>{$vatRate.Code|escape:'html'}</option>

                            {/foreach}
                            {/if}
                            </select>
                          </p>
                          
                          
                          <p>
                            <label class="fieldLabel" for="VATNumber" >{$page['Labels']['vat_number']|escape:'html'}:</label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="VATNumber" value="{$datarow.VATNumber|escape:'html'}" id="VATNumber" >
                        
                          </p>
                          
                         <p>
                            <hr>
                         </p>
                         
                          
                          
                    

                        
                         <p>
                            <label class="fieldLabel" >{$page['Labels']['opening_hours']|escape:'html'}</label>
                           
                             &nbsp;&nbsp; 
                             <label class="timeField"  >{$page['Labels']['opening_time']|escape:'html'}</label> 
                             <label class="timeField"  >{$page['Labels']['closing_time']|escape:'html'}</label> 
                        
                          </p>
    
                          <p>
                            <label class="fieldLabel" for="WeekdaysOpenTime" >{$page['Labels']['weekdays']|escape:'html'}:</label>
                           
                             &nbsp;&nbsp;<span style="padding-left:0px;margin-left:1px;" ><input  type="text" class="text timeField"  name="WeekdaysOpenTime" value="{$datarow.WeekdaysOpenTime|escape:'html'}" id="WeekdaysOpenTime" ></span>
                             <span><input  type="text" class="text timeField"  name="WeekdaysCloseTime" value="{$datarow.WeekdaysCloseTime|escape:'html'}" id="WeekdaysCloseTime" ></span>
                        
                          </p>
                          
                         
                          <p>
                            <label class="fieldLabel" for="SaturdayOpenTime" >{$page['Labels']['saturdays']|escape:'html'}:</label>
                           
                             &nbsp;&nbsp;<span style="padding-left:0px;margin-left:1px;" ><input  type="text" class="text timeField"  name="SaturdayOpenTime" value="{$datarow.SaturdayOpenTime|escape:'html'}" id="SaturdayOpenTime" ></span>
                             <span><input  type="text" class="text timeField"  name="SaturdayCloseTime" value="{$datarow.SaturdayCloseTime|escape:'html'}" id="SaturdayCloseTime" ></span>
                        
                          </p>
                          
                          
                          <p>
                            <label class="fieldLabel" for="SundayOpenTime" >{$page['Labels']['sundays']|escape:'html'}:</label>
                           
                             &nbsp;&nbsp;<span style="padding-left:0px;margin-left:1px;" ><input  type="text" class="text timeField"  name="SundayOpenTime" value="{$datarow.SundayOpenTime|escape:'html'}" id="SundayOpenTime" ></span>
                             <span><input type="text" class="text timeField"  name="SundayCloseTime" value="{$datarow.SundayCloseTime|escape:'html'}" id="SundayCloseTime" ></span>
                        
                          </p>
                          
                          
                         
                          <p>
                            <label class="fieldLabel" for="ServiceProvided" >{$page['Labels']['service_provided']|escape:'html'}:</label>
                           
                             &nbsp; <textarea class="text textArea"  name="ServiceProvided"  id="ServiceProvided" >{$datarow.ServiceProvided|escape:'html'}</textarea>
                        
                          </p>
                          
                          
                         
                           <p>
                            <label class="fieldLabel" for="SynopsisOfBusiness" >{$page['Labels']['synopsis_of_business']|escape:'html'}:</label>
                           
                            &nbsp; <textarea class="text textArea"  name="SynopsisOfBusiness"  id="SynopsisOfBusiness" >{$datarow.SynopsisOfBusiness|escape:'html'}</textarea>
                        
                          </p>
                          
                          
                          {if $datarow.NetworkID neq ''}
                          
                                <p>
                                    <hr>
                                </p>

                                <p>
                                    <label class="fieldLabel"  >{$page['Labels']['service_network']|escape:'html'}:</label>

                                    &nbsp;&nbsp;
                                    <label class="labelHeight"  >  
                                    {$datarow.NetworkName|escape:'html'}
                                     <input  type="hidden" class="text"  name="NetworkID" value="{$datarow.NetworkID|escape:'html'}" id="NetworkID" >
                                    </label> 

                                </p>


                                <p>
                                    <label class="fieldLabel" for="AccountNo" >{$page['Labels']['account_no']|escape:'html'}:</label>

                                    &nbsp;&nbsp;<input  type="text" class="text"  name="AccountNo" value="{$datarow.AccountNo|escape:'html'}" id="AccountNo" >

                                </p>
                                

                                <p>
                                    <label class="fieldLabel" for="SageSalesAccountNo" >{$page['Labels']['sage_sales_account_no']|escape:'html'}:</label>

                                    &nbsp;&nbsp;<input  type="text" class="text"  name="SageSalesAccountNo" value="{$datarow.SageSalesAccountNo|escape:'html'}" id="SageSalesAccountNo" >

                                </p>



                                <p>
                                    <label class="fieldLabel" for="SagePurchaseAccountNo" >{$page['Labels']['sage_purchase_account_no']|escape:'html'}:</label>

                                    &nbsp;&nbsp;<input  type="text" class="text"  name="SagePurchaseAccountNo" value="{$datarow.SagePurchaseAccountNo|escape:'html'}" id="SagePurchaseAccountNo" >

                                </p>


                                <p>
                                    <label class="fieldLabel" for="CompanyCode" >{$page['Labels']['company_code']|escape:'html'}:</label>

                                    &nbsp;&nbsp;<input  type="text" class="text"  name="CompanyCode" value="{$datarow.CompanyCode|escape:'html'}" id="CompanyCode" >

                                </p>


                                <p>
                                    <label class="fieldLabel" for="TrackingUsername" >{$page['Labels']['tracking_username']|escape:'html'}:</label>

                                    &nbsp;&nbsp;<input  type="text" class="text"  name="TrackingUsername" value="{$datarow.TrackingUsername|escape:'html'}" id="TrackingUsername" >

                                </p>


                                    <p>
                                    <label class="fieldLabel" for="TrackingPassword" >{$page['Labels']['tracking_password']|escape:'html'}:</label>

                                    &nbsp;&nbsp;<input  type="text" class="text"  name="TrackingPassword" value="{$datarow.TrackingPassword|escape:'html'}" id="TrackingPassword" >

                                </p>


                                    <p>
                                    <label class="fieldLabel" for="WarrantyUsername" >{$page['Labels']['warranty_username']|escape:'html'}:</label>

                                    &nbsp;&nbsp;<input  type="text" class="text"  name="WarrantyUsername" value="{$datarow.WarrantyUsername|escape:'html'}" id="WarrantyUsername" >

                                </p>


                                <p>
                                    <label class="fieldLabel" for="WarrantyPassword" >{$page['Labels']['warranty_password']|escape:'html'}:</label>

                                    &nbsp;&nbsp;<input  type="text" class="text"  name="WarrantyPassword" value="{$datarow.WarrantyPassword|escape:'html'}" id="WarrantyPassword" >

                                </p>


                                <p>
                                    <label class="fieldLabel" for="WarrantyAccountNumber" >{$page['Labels']['warranty_account_no']|escape:'html'}:</label>

                                    &nbsp;&nbsp;<input  type="text" class="text"  name="WarrantyAccountNumber" value="{$datarow.WarrantyAccountNumber|escape:'html'}" id="WarrantyAccountNumber" >

                                </p>


                                <p>
                                    <label class="fieldLabel" for="Area" >{$page['Labels']['area']|escape:'html'}:</label>

                                    &nbsp;&nbsp;<input  type="text" class="text"  name="Area" value="{$datarow.Area|escape:'html'}" id="Area" >

                                </p>


                                    <p>
                                    <label class="fieldLabel" for="WalkinJobsUpload" >{$page['Labels']['walkin_jobs_upload']|escape:'html'}:</label>

                                    &nbsp;&nbsp;<input  type="checkbox"   name="WalkinJobsUpload" {if $datarow.WalkinJobsUpload} checked="checked" {/if} id="WalkinJobsUpload" >

                                </p>

                           {else}
                               
                               <input  type="hidden" class="text"  name="NetworkID" value="" id="NetworkID" >
                               
                           {/if}
                           
                           
                           
                           <p>
                                <label class="fieldLabel" for="DefaultTravelTime" >{$page['Labels']['default_travel_time']|escape:'html'}:<sup>*</sup></label>

                                &nbsp;&nbsp;<input  type="text" class="text"  name="DefaultTravelTime" style="width:270px;" value="{$datarow.DefaultTravelTime|escape:'html'}" id="DefaultTravelTime" >
                                Mins
                           </p>
                           
                           <p>
                                <label class="fieldLabel" for="DefaultTravelSpeed" >{$page['Labels']['default_travel_speed']|escape:'html'}:<sup>*</sup></label>

                                &nbsp;&nbsp;<input  type="text" class="text"  name="DefaultTravelSpeed"  value="{$datarow.DefaultTravelSpeed|escape:'html'}" id="DefaultTravelSpeed" >
                           </p>
                           
                           
                           
                          {if $datarow.ServiceProviderID neq '' && $datarow.ServiceProviderID neq '0' && $nClients|@count gt 0} 
                           
                              
                              
                              <p>
                                <table style="padding:0px;margin:0px" id="ClientsList" >
                                    <tr>
                                        <td width="180px" ><b>Client</b></td>
                                        <td width="120px" ><b>Phone Number</b></td>
                                        <td width="155px" ><b>Email Address</b></td>
                                        <td width="20px" ></td>
                                    </tr>
                                    
                                    
                                    {foreach $clientContacts as $cc}
                                        
                                        
                                        <tr id='C_{$cc.ClientID}'  ><td><input type='hidden' name='ClientIDs[]' value='{$cc.ClientID}' >{$cc.ClientName|escape:'html'}</td><td><input type='hidden' name='ClientContactPhones[]' value='{$cc.ContactPhone|escape:'html'}' >{$cc.ContactPhone|escape:'html'}</td><td><input type='hidden' name='ClientContactEmails[]' value='{$cc.ContactEmail|escape:'html'}' >{$cc.ContactEmail|escape:'html'}</td><td><span><img style='cursor:pointer;' class='CloseImage'  src='{$_subdomain}/css/Skins/{$_theme}/images/close.png' width='17' height='16' ></span></td></tr>
                                        
                                 
                                    {/foreach}
                                    
                                    
                                </table>  
                              </p>
                              <p>
                                
                                 <select name="ClientID" id="ClientID" class="text" style="width:200px;" >
                                    <option value="" >{$page['Text']['select_default_option']|escape:'html'}</option>

                                    {foreach $nClients as $client}

                                        <option value="{$client.ClientID}" >{$client.ClientName|escape:'html'}</option>

                                    {/foreach}
                                </select>
                                &nbsp;&nbsp;
                                <input  type="text" class="text"  name="ClientContactPhone" style="width:130px;"  value="" id="ClientContactPhone" >
                                &nbsp;&nbsp;
                                <input  type="text" class="text"  name="ClientContactEmail" style="width:150px;" value="" id="ClientContactEmail" >
                                <span>
                                <img src="{$_subdomain}/css/Skins/{$_theme}/images/add_icon.png" alt="{$page['Text']['add_client']|escape:'html'}" title="{$page['Text']['add_client']|escape:'html'}" width="20" height="20" id="AddClientDetails" style="cursor:pointer;" >
                                </span>
                              </p>
                              
                              
                              
                              
                              
                              
                          {/if} 
                                 
                          <p>
                            <label class="fieldLabel" for="AutoReorderStock" >{$page['Labels']['AutoReorderStock']|escape:'html'} - Samsung:</label>
                           
                             &nbsp;&nbsp; 
                        
                                
                             <input style="margin-top:12px"  type="checkbox" name="AutoReorderStock"  value="Yes" {if $datarow.AutoReorderStock eq 'Yes'} checked="checked" {/if}  /> 
                                  

                                
                          </p>
                          <p>
                            <label class="fieldLabel" for="ChangeSupplieronJobPartOrder" >{$page['Labels']['ChangeSupplieronJobPartOrder']|escape:'html'}:</label>
                           
                             &nbsp;&nbsp; 
                        
                                
                             <input style="margin-top:12px"  type="checkbox" name="ChangeSupplieronJobPartOrder"  value="Yes" {if $datarow.ChangeSupplieronJobPartOrder|default:'' eq 'Yes'} checked="checked" {/if}  /> 
                                  

                                
                          </p>
                          
                          <p>
                            <label class="fieldLabel" for="EmailServiceInstruction" >{$page['Labels']['email_service_instruction']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; 
                        
                                
                                  <input  type="radio" name="EmailServiceInstruction"  value="Active" {if $datarow.EmailServiceInstruction eq 'Active'} checked="checked" {/if}  /> <span class="text" >{$page['Labels']['active']|escape:'html'}</span> 
                                  <input  type="radio" name="EmailServiceInstruction"  value="In-Active" {if $datarow.EmailServiceInstruction eq 'In-Active'} checked="checked" {/if}  /> <span class="text" >{$page['Labels']['inactive']|escape:'html'}</span> 

                                
                          </p>
                          
                          
                          
                          
                          <p>
                            <label class="fieldLabel" for="CourierStatus" >{$page['Labels']['courier_status']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; 
                        
                                {foreach $statuses as $status}

                                    <input  type="radio" name="CourierStatus"  value="{$status.Code}" {if $datarow.CourierStatus eq $status.Code} checked="checked" {/if}  /> <span class="text" >{$status.Name|escape:'html'}</span> 

                                {/foreach}    
                          </p>
                                                                            
                          <p>
                            <label class="fieldLabel" for="Status" >{$page['Labels']['sp_status']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; 
                        
                                {foreach $statuses as $status}

                                    <input  type="radio" name="Status"  value="{$status.Code}" {if $datarow.Status eq $status.Code} checked="checked" {/if}  /> <span class="text" >{$status.Name|escape:'html'}</span> 

                                {/foreach}    
                          </p>

                           
                          <p>
                            <label class="fieldLabel" for="OnlineDiary" >{$page['Labels']['online_diary']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; 
                        
                                
                                  <input onclick="checkDiaryType();" id="diaryActiveID"  type="radio" name="OnlineDiary"  value="Active" {if $datarow.OnlineDiary eq 'Active'} checked="checked" {/if}  /> <span class="text" >{$page['Labels']['active']|escape:'html'}</span> 
                                  <input onclick="checkDiaryType();" type="radio" name="OnlineDiary"  value="In-Active" {if $datarow.OnlineDiary eq 'In-Active'} checked="checked" {/if}  /> <span class="text" >{$page['Labels']['inactive']|escape:'html'}</span> 

                                  
                          </p>
                          <p id="diaryTypeP" {if $datarow.OnlineDiary eq 'Active'}style="display:block;"{else}style="display:none"{/if}>
                            <label class="fieldLabel" for="OnlineDiary" >Diary Type:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; 
                        
                               
                                  <input id="FullViamente" onclick="verify(this);"  type="radio" name="DiaryType"  value="FullViamente"   {if $datarow.DiaryType eq 'FullViamente'}     checked="checked" {/if}  /> <span class="text" >Viamente</span> 
                                  <input id="NoViamente" onclick="verify(this);"  type="radio" name="DiaryType"  value="NoViamente"     {if $datarow.DiaryType eq 'NoViamente'}  checked="checked" {/if}  /> <span class="text" >Stand Alone</span> 
                                  <input id="AllocationOnly" onclick="verify(this);"  type="radio" name="DiaryType"  value="AllocationOnly" {if $datarow.DiaryType eq 'AllocationOnly'}  checked="checked"{/if}  /> <span class="text" >Alloc. Only</span> 

                                  <input {if isset($datarow.DiaryAllocationType)&&$datarow.DiaryAllocationType eq 'GridMapping'}checked=checked{/if} type="checkbox" name="DiaryAllocationType" id="allocationOnly"> <span>Grid System</span>
                          </p>
                          
                     <p>
                            <label class="fieldLabel" for="ServiceProviderType" >{$page['Labels']['ServiceProviderType']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; 
                        
                                
                                  <input   type="radio" name="ServiceProviderType"  value="Standard" {if $datarow.ServiceProviderType eq 'Standard'} checked="checked" {/if}  /> <span class="text" >{$page['Labels']['Standard']|escape:'html'}</span> 
                                  <input   type="radio" name="ServiceProviderType"  value="Branch" {if $datarow.ServiceProviderType eq 'Branch'} checked="checked" {/if}  /> <span class="text" >{$page['Labels']['Branch']|escape:'html'}</span> 

                                  
                     </p>
                     
		     
		     <p>
			<label class="fieldLabel" for="SBRASync">Service Base RA Sync:<sup>*</sup>
			</label>
			&nbsp;&nbsp; 
			<input type="radio" name="SBRASync" value="Yes" {if $datarow.SBRASync == "Yes"}checked="checked"{/if} />
			<span class="text">Yes</span>
			<input type="radio" name="SBRASync" value="No" {if $datarow.SBRASync == "No"}checked="checked"{/if} />
			<span class="text">No</span>
		     </p>
		     
		     
                           <p>
                    
                                <span class= "bottomButtons" >

                                    <input id="ServiceProviderID" type="hidden" name="ServiceProviderID"  value="{$datarow.ServiceProviderID|escape:'html'}" >
                                 
                                    {if $datarow.ServiceProviderID neq '' && $datarow.ServiceProviderID neq '0'}
                                        
                                         <input type="submit" name="update_save_btn" class="textSubmitButton centerBtn" id="update_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                                    
                                    {else}
                                        
                                        <input type="submit" name="insert_save_btn" class="textSubmitButton centerBtn" id="insert_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                                        
                                    {/if}
                                   
                                        <br>
                                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >



                                </span>

                            </p>
                            <p>
			    <div style="padding-top:12px;"><input type="button" name="AuthManButton" class="textSubmitButton rightBtn" id="AuthManButton" value="{$page['Labels']['authorised_manufacturers']|escape:'html'}" ></div>
                                {* <div id="AuthManButton" class="textSubmitButton centerBtn">{$page['Labels']['authorised_manufacturers']|escape:'html'}</div> *}
                            </p>
                           
                </fieldset>    
                        
                </form>        
                                             
       
</div>
                                        
<div id="AuthoriseManufacturersFormPanel" class="SystemAdminFormPanel" style="display: none;" >
    <form id="AuthoriseManufacturersForm" name="AuthoriseManufacturersForm" method="post"  action="#" class="inline">
        <fieldset>
            <legend title="" >{$page['Text']['authman_legend']|escape:'html'}</legend>
            
            <div style="text-align: right" >
                <span class="text" >{$page['Labels']['show_authorised_tagged']|escape:'html'}</span><input type="checkbox" id="authman_show_tagged" name="authman_show_tagged"  value="1" />
            </div>
            
            <div style="text-align: right" >
                <span class="text" >{$page['Labels']['show_datacheck_tagged']|escape:'html'}</span><input type="checkbox" id="checkman_show_tagged" name="checkman_show_tagged"  value="1" />
            </div>

                <table id="AuthorisedManufacturersTable" border="0" cellpadding="0" cellspacing="0" class="browse">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Authorised ASC</th>
                            <th>Data Checked</th>
                            <th>Key</th>
                            <th>orig_auth</th>
                            <th>orig_check</th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach $manufacturers as $manufacturer}
                        <tr>
                            <td>{$manufacturer.ManufacturerName|escape:'html'}</td>
                            <td>{$manufacturer.Auth}</td>
                            <td>{$manufacturer.DataChecked}</td>
                            <td>{$manufacturer.ManufacturerID}</td>
                            <td>{$manufacturer.Auth}</td>
                            <td>{$manufacturer.DataChecked}</td>
                        </tr>
                        {/foreach}
                    </tbody>
                </table> 
            
            <br />
 
            <p>
                <span class= "bottomButtons" >
                    <input type="submit" name="authman_save_btn" class="textSubmitButton centerBtn" id="authman_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                    <br />
                    <input type="submit" name="authman_cancel_btn" class="textSubmitButton rightBtn" id="authman_cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >
                </span>
            </p>
        </fieldset>
    </form>
</div>
                 
{/if}  
                                    
                                    
{* This block of code is for to display message after data updation *} 

                                    
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>    

    
 
  {* This block of code is for to display message after data insertion *}                      
                        
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_inserted_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>                            
                        
