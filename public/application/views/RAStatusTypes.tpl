{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $RAStatusTypes}
{/block}
    
    
{block name=afterJqueryUI}
    <script src="{$_subdomain}/js/jpicker-1.1.6.js" type="text/javascript"></script>
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script> 
    <link rel="Stylesheet" type="text/css" href="{$_subdomain}/css/themes/pccs/jPicker-1.1.6.css" />
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
    <style type="text/css" >
	.ui-combobox-input {
	     width:300px;
	 }  
    </style>
    <script type="text/javascript">
	$.fn.jPicker.defaults.images.clientPath = "{$_subdomain}/css/themes/pccs/images/colorPicker/";
    </script>
{/block}
    

{block name=scripts}
    
    <script type="text/javascript" src="{$_subdomain}/js/functions.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/TableTools.min.js"></script>
    
    <script type="text/javascript">
    
    var copy = false;
    
    var table;
    
    var $statuses = [
	{foreach from=$statuses item=st}
	   ["{$st.Name}", "{$st.Code}"],
	{/foreach}
    ]; 
    
    
    function gotoEditPage($sRow) {
        
        $("#updateButtonId").removeAttr("disabled").removeClass("gplus-blue-disabled").addClass("gplus-blue");
        $("#updateButtonId").trigger("click");
	
    }
    
    
    function inactiveRow(nRow, aData) {
        
        if (aData[7]) {
	    $("td:eq(3)", nRow).html("<div style='width:60px;background-color:#" + aData[7] + ";color:#" + aData[7] + ";' >" + aData[7] + "</div>");
        }
        
        if (aData[8] == $statuses[1][1]) {  
            $(nRow).addClass("inactive");
            //$("td:eq(4)", nRow).html( $statuses[1][0]);
        } else {
            $(nRow).addClass("");
	    //$("td:eq(4)", nRow).html( $statuses[0][0]);
        }
	
    }
    

    function gotoInsertPage($sRow) {
       
        $("#copyRowId").val($sRow[0]);
        $("#addButtonId").trigger("click");
        $("#copyRowId").val("");
        
    }
    
    
    $(document).ready(function() {


	if (getUrlVars()["authType"]) {
	    $("input[name=authType][value=" + getUrlVars()["authType"] + "]").attr("checked", true);
	}
	
	
	if (getUrlVars()["active"] && getUrlVars()["active"] == "1") {
	    $("input[name=active][value=active]").attr("checked", true);
	} else {
	    $("input[name=active][value=all]").attr("checked", true);
	}
	

	//Click handler for finish button.
	$(document).on('click', '#finish_btn', function() {
	    $(location).attr('href', '{$_subdomain}/SystemAdmin/index/lookupTables');
	});


	$(document).on("click", "input[name=active]", function() {
	    if ($(this).val() == "active") {
		location.href = "{$_subdomain}/LookupTables/RAStatusTypes/" + (getUrlVars()["authType"] ? getUrlVars()["authType"] : "") + "/?active=1";
	    } else {
		location.href = "{$_subdomain}/LookupTables/RAStatusTypes/" + (getUrlVars()["authType"] ? getUrlVars()["authType"] : "");
	    }
	});


	/* =======================================================
	 *
	 * set tab on return for input elements with form submit on auto-submit class...
	 *
	 * ======================================================= */

	$('input[type=text],input[type=password]').keypress(function(e) {
	    if (e.which == 13) {
		$(this).blur();
                if ($(this).hasClass('auto-submit')) {
		    $('.auto-hint').each(function() {
			$this = $(this);
                        if($this.val() == $this.attr('title')) {
			    $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                            if($this.hasClass('auto-pwd')) {
				$this.prop('type','password');
                            }
                        }
		    });
                    $(this).get(0).form.onsubmit();
		} else {
		    $next = $(this).attr('tabIndex') + 1;
		    $('[tabIndex="'+$next+'"]').focus();
                }
                return false;
	    }
	});        

	{if $SupderAdmin == true} 
	    var  displayButtons = "UPA";
	{else}
	    var displayButtons =  "UP";
	{/if} 
	    
	table = $("#RASTResults").PCCSDataTable({
	    aoColumns: [ 
		/* RAStatusID   */  { bVisible: false },    
		/* RAStatusCode */  { sWidth:   "140px" },
		/* RAStatusName */  null,
		/* ListOrder    */  null,
		/* Colour       */  null,
		/* Status	*/  null,
		/* Brand	*/  null,
		/* Brand	*/  null,
		/* Brand	*/  null,
		/* Brand	*/  null,
		/* Brand ID	*/  null
	    ],
            
	    aaSorting:		[[3, "asc"]],
	    displayButtons:	displayButtons,
	    addButtonId:	'addButtonId',
	    addButtonText:	'{$page['Buttons']['insert']|escape:'html'}',
	    createFormTitle:	'{$page['Text']['insert_page_legend']|escape:'html'}',
	    createAppUrl:	'{$_subdomain}/LookupTables/RAStatusTypes/insert/',
	    createDataUrl:	'{$_subdomain}/LookupTables/ProcessData/RAStatusTypes/',
	    formInsertButton:	'insert_save_btn',
	    copyInsertRowId:	'copyRowId',
	    bServerSide:	false,
	    
	    frmErrorRules: {
		BrandID:	    { required: true },
		AuthorisationType:  { required: function() { return($("#AuthorisationType").length != 0) }},
		RAStatusName:	    { required: true },  
		ListOrder:	    { required: true, digits: true },
		Colour:		    { required: true },     
		Status:		    { required: true },
		SubsetOnly:	    { required: true },
		ServiceBaseStatus:  { required: true },
		Final:		    { required: true }
	    },

            frmErrorMessages: {
		BrandID:	    { required: "{$page['Text']['brand_error']|escape:'html'}" },
		AuthorisationType:  { required: "{$page['Text']['auth_type_error']|escape:'html'}" },
		RAStatusName:	    { required: "{$page['Text']['status_name_error']|escape:'html'}" },  
		ListOrder:	    { required: "{$page['Text']['system_list_order_error']|escape:'html'}" },
		Colour:		    { required: "{$page['Errors']['colour_error']|escape:'html'}" },    
		Status:		    { required: "{$page['Text']['status_error']|escape:'html'}" },
		SubsetOnly:	    { required: "{$page['Text']['subset_only_error']|escape:'html'}" },   
		ServiceBaseStatus:  { required: "{$page['Text']['sb_error']|escape:'html'}" },
		Final:		    { required: "Final resolution status is required." }
	    },                     

	    popUpFormWidth:	715,
	    popUpFormHeight:	430,
	    pickButtonId:	"copy_btn",
	    pickButtonText:	"{$page['Buttons']['copy']|escape:'html'}",
	    pickCallbackMethod: "gotoInsertPage",
	    colorboxForceClose: false,
	    updateButtonId:	'updateButtonId',
	    updateButtonText:	'{$page['Buttons']['edit']|escape:'html'}',
	    updateFormTitle:	'{$page['Text']['update_page_legend']|escape:'html'}',
	    updateAppUrl:	'{$_subdomain}/LookupTables/RAStatusTypes/update/',
	    updateDataUrl:	'{$_subdomain}/LookupTables/ProcessData/RAStatusTypes/',
	    formUpdateButton:	'update_save_btn',
	    colorboxFormId:	"RASTForm",
	    frmErrorMsgClass:	"fieldError",
	    frmErrorElement:	"label",
	    htmlTablePageId:	'RASTResultsPanel',
	    htmlTableId:	'RASTResults',
	    fetchDataUrl:	'{$_subdomain}/LookupTables/ProcessData/RAStatusTypes/fetch/' + (getUrlVars()["authType"] ? getUrlVars()["authType"] : "") + (getUrlVars()["active"] ? ("/?active=" + getUrlVars()["active"]) : ""),
	    formCancelButton:	'cancel_btn',
	    dblclickCallbackMethod: 'gotoEditPage',
	    fnRowCallback:	'inactiveRow',
	    searchCloseImage:	'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
	    tooltipTitle:	"{$page['Text']['tooltip_title']|escape:'html'}",
	    iDisplayLength:	25,
	    formDataErrorMsgId: "suggestText",
	    frmErrorSugMsgClass:"formCommonError"
	});
	
	/*
	$(document).on("click", "#copy_btn", function() {
	    copy = true;
	});
	$(document).on("click", "#copy_btn, #updateButtonId", function() {
	    copy = true;
	});
	$(document).on("dblclick", function() {
	    copy = false;
	});
	
	$(document).on("dblclick", function() {
	    console.log($($($(".row_selected").find("td"))[5]).text());
	    if($($($(".row_selected").find("td"))[5]).text() == "Skyline") {
		$.colorbox.close();
		return false;
	    }
	});
	*/
	
	$(document).on("change", "input[name=authType]", function() {
	    location.href = "{$_subdomain}/LookupTables/RAStatusTypes/?authType=" + $(this).val();
	});
	
    });

    </script>

    {/block}

    {block name=body}

    <div class="breadcrumb">
        <div>
            <a href="{$_subdomain}/SystemAdmin">{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/lookupTables">{$page['Text']['lookup_tables']|escape:'html'}</a> / &nbsp;{$page['Text']['page_title']|escape:'html'}
        </div>
    </div>

    <div class="main" id="home">

	<div class="LTTopPanel">
	    
	     <form id="RAStatusTypesForm" name="RAStatusTypesForm" method="post" action="#" class="inline">
		 <fieldset>
		    <legend title="">{$page['Text']['legend']|escape:'html'}</legend>
		    <p>
			<label>{$page['Text']['description']|escape:'html'}</label>
		    </p> 
		 </fieldset> 
	     </form>
		    
	</div>  


	<div class="LTResultsPanel" id="RASTResultsPanel">

	    <div id="authTypes" style="margin-bottom:5px;">
		{foreach $authTypes as $type}
		    <input type="radio" name="authType" value="{$type.AuthorisationTypeID}" /> 
		    <span style="margin-right:10px;">{$type.AuthorisationTypeName}</span>
		{/foreach}
		<div style="display:block; position:relative; float:right;">
		    <input type="radio" name="active" value="all" /> Show all
		    <input type="radio" name="active" value="active" /> Show active only
		</div>
	    </div>
	    
	    <table id="RASTResults" border="0" cellpadding="0" cellspacing="0" class="browse">
		<thead>
		    <tr>
			<th></th> 
			<th width="8%" title="{$page['Text']['status_id']|escape:'html'}">{$page['Text']['status_id']|escape:'html'}</th>
			<th width="21%" title="{$page['Text']['status_name']|escape:'html'}">{$page['Text']['status_name']|escape:'html'}</th>
			<th width="9%" title="{$page['Text']['list_order']|escape:'html'}">{$page['Text']['list_order']|escape:'html'}</th>
			<th width="10%" title="Subset Only">Subset Only</th>
			<th width="6%" title="Final">Final</th>
			<th width="11%" title="Subset Count">Subset Count</th>
			<th width="7%" title="{$page['Text']['colour']|escape:'html'}">{$page['Text']['colour']|escape:'html'}</th>
			<th width="7%" title="{$page['Text']['status']|escape:'html'}">{$page['Text']['status']|escape:'html'}</th>
			<th width="13%" title="{$page['Text']['brand']|escape:'html'}">{$page['Text']['brand']|escape:'html'}</th>
			<th width="8%" title="{$page['Text']['brand_id']|escape:'html'}">{$page['Text']['brand_id']|escape:'html'}</th>
		    </tr>
		</thead>
		<tbody>
		</tbody>
	    </table>  

	</div>        

	<div class="bottomButtonsPanel" >
	    <hr/>
	    <button id="finish_btn" type="button" class="gplus-blue rightBtn"><span class="label">{$page['Buttons']['finish']|escape:'html'}</span></button>
	</div>        

	<input type="hidden" name="copyRowId" id="copyRowId" /> 

	{if $SupderAdmin eq false} 
	    <input type="hidden" name="addButtonId" id="addButtonId"> 
	{/if} 

    </div>

{/block}