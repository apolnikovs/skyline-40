{if $accessErrorFlag eq true} 
    
    <div id="accessDeniedMsg" class="SystemAdminFormPanel"  >   
    
        <form id="accessDeniedMsgForm" name="accessDeniedMsgForm" method="post"  action="#" class="inline">
                <fieldset>
                    <legend title="" > {$page['Text']['error_page_legend']|escape:'html'} </legend>
                    <p>

                        <label class="formCommonError" >{$accessDeniedMsg|escape:'html'}</label><br><br>

                    </p>

                    <p>

                        <span class= "bottomButtons" >


                            <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                        </span>

                    </p>


                </fieldset>   
        </form>            


    </div>    
    
{else}  
    
    {if $datarow.PostcodeAllocationID eq '' || $datarow.PostcodeAllocationID eq '0'}
        
    <link rel="stylesheet" href="{$_subdomain}/js/plupload/jquery.plupload.queue/css/jquery.plupload.queue.css" type="text/css" media="screen" />
    
    <script type="text/javascript" src="{$_subdomain}/js/plupload/plupload.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/plupload/plupload.gears.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/plupload/plupload.html5.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/plupload/jquery.plupload.queue/jquery.plupload.queue.js"></script>
    
    <script type="text/javascript" >
              
        
    $(document).ready(function() {
        $("#NetworkID").combobox({
            change: function() {
                $manufacturerDropDownList = $clientDropDownList = $serviceProviderDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';
                var $NetworkID = $("#NetworkID").val();
                if($NetworkID && $NetworkID!='')
                {
                   //Getting clients for selected network.   
                   $.post("{$_subdomain}/JobAllocation/postcodeAllocations/getClients/"+urlencode($NetworkID), '', function(data){
                        var $networkClients = eval("(" + data + ")");
                        if($networkClients)
                        {
                            for(var $i=0;$i<$networkClients.length;$i++)
                            {
                                $clientDropDownList += '<option value="'+$networkClients[$i]['ClientID']+'" >'+$networkClients[$i]['ClientName']+'</option>';
                            }
                        }
                        $("#ClientID").html('');
                        $("#ClientID").html($clientDropDownList);
                    });
                   //Getting manufacturer for selected network.   
                   $.post("{$_subdomain}/JobAllocation/postcodeAllocations/getManufacturers/"+urlencode($NetworkID), '', function(data){
                        var $networkManufacturers = eval("(" + data + ")");
                        if($networkManufacturers)
                        {
                            for(var $i=0;$i<$networkManufacturers.length;$i++)
                            {
                                $manufacturerDropDownList += '<option value="'+$networkManufacturers[$i]['ManufacturerID']+'" >'+$networkManufacturers[$i]['ManufacturerName']+'</option>';
                            }
                        }
                        $("#ManufacturerID").html('');
                        $("#ManufacturerID").html($manufacturerDropDownList);
                    });
                }
                else 
                {
                    $("#ClientID").html('');
                    $("#ManufacturerID").html('');
                    $("#ClientID").html($clientDropDownList);
                    $("#ManufacturerID").html($manufacturerDropDownList);
                }
            }
        });
        $("#RepairSkillID").combobox();
        $("#ManufacturerID").combobox();
        $("#JobTypeID").combobox({
            change: function() {
                $serviceTypeDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';
                var $JobTypeID = $("#JobTypeID").val();
                if($JobTypeID && $JobTypeID!='')
                {
                    //Getting service types for selected network, client and jobtype.   
                    $.post("{$_subdomain}/JobAllocation/postcodeAllocations/getServiceTypes/"+urlencode($JobTypeID)+"/"+urlencode($("#NetworkID").val())+"/"+urlencode($("#ClientID").val()), '', function(data){
                        var $serviceTypes = eval("(" + data + ")");
                        if($serviceTypes)
                        {
                            for(var $i=0;$i<$serviceTypes.length;$i++)
                            {
                                $serviceTypeDropDownList += '<option value="'+$serviceTypes[$i]['ServiceTypeID']+'" >'+$serviceTypes[$i]['Name']+'</option>';
                            }
                        }
                        $("#ServiceTypeID").html('');
                        $("#ServiceTypeID").html($serviceTypeDropDownList);
                    });
                }
                else 
                {
                    $("#ServiceTypeID").html('');
                    $("#ServiceTypeID").html($serviceTypeDropDownList);
                }
                $("#RepairSkillID").trigger("change");
            }
        });
        $("#ClientID").combobox({
            change: function() {
                $("#JobTypeID").trigger("change");
            }
        });
        $("#ServiceTypeID").combobox();
    
            // Setup html4 version
            $("#html4_uploader").pluploadQueue({
                    // General settings
                    runtimes : 'html5',
                    url : '{$_subdomain}/JobAllocation/postcodeAllocations/uploadFiles/{$fileUploadPrefix}',
                    filters : [
                            { title : "CSV files", extensions : "csv" }
                    ]
            });
    
                
            /* =======================================================
            *
            * Initialise input auto-hint functions...
            *
            * ======================================================= */

            $('.auto-hint').focus(function() {
                    $this = $(this);
                    if ($this.val() == $this.attr('title')) {
                        $this.val('').removeClass('auto-hint');
                        if ($this.hasClass('auto-pwd')) {
                            $this.prop('type','password');
                        }
                    }
                } ).blur(function() {
                    $this = $(this);
                    if ($this.val() == '' && $this.attr('title') != '')  {
                        $this.val($this.attr('title')).addClass('auto-hint');
                        if ($this.hasClass('auto-pwd')) {
                            $this.prop('type','text');
                        }
                    }         
                } ).each(function(){
                    $this = $(this);
                    if ($this.attr('title') == '') { return; }
                    if ($this.val() == '') { 
                        if ($this.attr('type') == 'password') {
                            $this.addClass('auto-pwd').prop('type','text');
                        }
                        $this.val($this.attr('title')); 
                    } else { 
                        $this.removeClass('auto-hint'); 
                    }
                    $this.attr('autocomplete','off');
                } ); 
                
                
                
                
                //change handler for network dropdown box.
                /*$(document).on('change', '#NetworkID', 
                            function() {

                                       

                                 
                                    $manufacturerDropDownList = $clientDropDownList = $serviceProviderDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';

                                    var $NetworkID = $("#NetworkID").val();

                                    if($NetworkID && $NetworkID!='')
                                    {
                                       
                                       //Getting clients for selected network.   
                                       $.post("{$_subdomain}/JobAllocation/postcodeAllocations/getClients/"+urlencode($NetworkID),        

                                        '',      
                                        function(data){
                                                var $networkClients = eval("(" + data + ")");
    
                                                if($networkClients)
                                                {
                                                     
                                                    for(var $i=0;$i<$networkClients.length;$i++)
                                                    {
                                                        
                                                        $clientDropDownList += '<option value="'+$networkClients[$i]['ClientID']+'" >'+$networkClients[$i]['ClientName']+'</option>';
                                                    }
                                                }
                                                
                                                
                                                $("#ClientID").html('');
                                                $("#ClientID").html($clientDropDownList);

                                        });
                                        
                                        
                                        
                                       //Getting manufacturer for selected network.   
                                       $.post("{$_subdomain}/JobAllocation/postcodeAllocations/getManufacturers/"+urlencode($NetworkID),        

                                        '',      
                                        function(data){
                                                var $networkManufacturers = eval("(" + data + ")");
    
                                                if($networkManufacturers)
                                                {
                                                     
                                                    for(var $i=0;$i<$networkManufacturers.length;$i++)
                                                    {
                                                        
                                                        $manufacturerDropDownList += '<option value="'+$networkManufacturers[$i]['ManufacturerID']+'" >'+$networkManufacturers[$i]['ManufacturerName']+'</option>';
                                                    }
                                                }
                                                
                                                 $("#ManufacturerID").html('');
                                                
                                                $("#ManufacturerID").html($manufacturerDropDownList);

                                        });
                                        
                                        
                                        
                                    }
                                    else 
                                    {
                                        $("#ClientID").html('');
                                        $("#ManufacturerID").html('');
                                       
                        
                                        $("#ClientID").html($clientDropDownList);
                                        $("#ManufacturerID").html($manufacturerDropDownList);
                                        
                                    }
                                              

                            });*/    
                            
                            
                //change handler for job type dropdown box.
                /*$(document).on('change', '#JobTypeID', 
                            function() { 
                            
                                   $serviceTypeDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';

                                    var $JobTypeID = $("#JobTypeID").val();

                                    if($JobTypeID && $JobTypeID!='')
                                    {
                                       
                                       //Getting service types for selected network, client and jobtype.   
                                       $.post("{$_subdomain}/JobAllocation/postcodeAllocations/getServiceTypes/"+urlencode($JobTypeID)+"/"+urlencode($("#NetworkID").val())+"/"+urlencode($("#ClientID").val()),        

                                        '',      
                                        function(data){
                                                var $serviceTypes = eval("(" + data + ")");
    
                                                if($serviceTypes)
                                                {
                                                     
                                                    for(var $i=0;$i<$serviceTypes.length;$i++)
                                                    {
                                                        
                                                        $serviceTypeDropDownList += '<option value="'+$serviceTypes[$i]['ServiceTypeID']+'" >'+$serviceTypes[$i]['Name']+'</option>';
                                                    }
                                                }
                                                
                                                $("#ServiceTypeID").html('');
                                                
                                                $("#ServiceTypeID").html($serviceTypeDropDownList);

                                        });
                                        
                                        
                                    }
                                    else 
                                    {
                                        $("#ServiceTypeID").html('');
                                        
                                        $("#ServiceTypeID").html($serviceTypeDropDownList);
                                        
                                    }
                            
                            
                                    $("#RepairSkillID").trigger("change");
                            
                             });*/  
                             
                             
                  //change handler for client dropdown box.
                  /*$(document).on('change', '#ClientID', 
                            function() {
                            
                                $("#JobTypeID").trigger("change");
                              
                             });*/  
                             
                   
                   //click handler for save button.
                   $(document).on('click', '#insert_save_btn', 
                            function() {
                            
                                    if($(".plupload_disabled").length>0)
                                    {
                                            return true;
                                    }       
                                    else
                                    {
                                        alert("Please upload files first.  To upload files please click on 'Start upload' button.");
                                        return false;
                                    } 
                                 
                             }); 
                   
                   
                   
                            
                
         });       
        
    </script>
    {/if}
    
    <div id="PostcodeAllocationsFormPanel" class="SystemAdminFormPanel" >
    
                <form id="PostcodeAllocationsForm" name="PostcodeAllocationsForm" method="post" enctype="multipart/form-data" action="#" class="inline" >
       
                <fieldset>
                    <legend title="" >{$form_legend|escape:'html'}</legend>
                      
                    
                    
                       {if $datarow.PostcodeAllocationID neq '' && $datarow.PostcodeAllocationID neq '0'}

                             
                           
                            

                                <p>
                                    <label class="fieldLabel"  >{$page['Labels']['network']|escape:'html'}:</label>
                                    &nbsp;&nbsp;  
                                    <label class="labelHeight"  >  
                                    {$datarow.NetworkName|escape:'html'}
                                    </label> 
                                </p>

                                <p>
                                        <label class="fieldLabel" >{$page['Labels']['repair_skill']|escape:'html'}:</label>

                                        &nbsp;&nbsp;  
                                        <label class="labelHeight"  >  
                                       {$datarow.RepairSkillName|escape:'html'}
                                        </label> 
                                </p>


                                <p>
                                        <label class="fieldLabel" for="ManufacturerID" >{$page['Labels']['manufacturer']|escape:'html'}:</label>

                                        &nbsp;&nbsp;  
                                        <label class="labelHeight" >  
                                       {$datarow.ManufacturerName|escape:'html'}
                                        </label> 
                                </p>


                                <p>
                                        <label class="fieldLabel" for="JobTypeID" >{$page['Labels']['job_type']|escape:'html'}:</label>

                                       
                                        &nbsp;&nbsp;  
                                        <label class="labelHeight" >  
                                       {$datarow.Type|escape:'html'}
                                        </label> 
                                </p>

                                <p>
                                        <label class="fieldLabel" for="ClientID" >{$page['Labels']['client']|escape:'html'}:</label>

                                        &nbsp;&nbsp;  
                                         <label class="labelHeight" >  
                                       {$datarow.ClientName|escape:'html'}
                                        </label> 
                                </p>


                                <p>
                                        <label class="fieldLabel" for="ServiceTypeID" >{$page['Labels']['service_type']|escape:'html'}:</label>

                                        &nbsp;&nbsp;  
                                        <label class="labelHeight" >  
                                       {$datarow.ServiceTypeName|escape:'html'}
                                        </label> 
                                </p>


                                <p>
                                    <label class="fieldLabel" for="Status" >{$page['Labels']['out_of_area']|escape:'html'}:</label>

                                    &nbsp;&nbsp; 

                                     <label class="labelHeight" >  
                                       {$datarow.OutOfArea|escape:'html'}
                                     </label> 


                                </p>
                                
                                 <p>
                                    <label class="fieldLabel" for="Status" >{$page['Labels']['postcode_areas']|escape:'html'}:</label>

                                    &nbsp;&nbsp; 

                                     <span class="postCodesBox" >  
                                       {$datarow.PostCodeAreas|escape:'html'}
                                     </span>
                                </p>
                           
                    
                       {else}
                    
                    
                       <p><label id="suggestText" ></label></p>
                            
                        <p>
                               <label ></label>
                               <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>
                        </p>
                       
                         
                         
                        
                        
                         {if $SupderAdmin eq true} 
                         
                          
                          <p>
                            <label class="fieldLabel" for="NetworkID" >{$page['Labels']['network']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp;  
                             <select name="NetworkID" id="NetworkID" class="text" >
                                <option value="" {if $datarow.NetworkID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $networks as $network}

                                    <option value="{$network.NetworkID}" {if $datarow.NetworkID eq $network.NetworkID}selected="selected"{/if}>{$network.CompanyName|escape:'html'}</option>

                                {/foreach}
                                
                            </select>
                          </p>
                          
                        {else}
                        
                         <p>
                            <label class="fieldLabel" for="NetworkID" >{$page['Labels']['network']|escape:'html'}:<sup>*</sup></label>
                             &nbsp;&nbsp;  
                            <label class="labelHeight"  >  
                             {$datarow.NetworkName|escape:'html'}
                             <input type="hidden" name="NetworkID" id="NetworkID" value="{$datarow.NetworkID|escape:'html'}" >
                            </label> 
                          </p>
                          
                        {/if}    
                        
                        
                        <p>
                                <label class="fieldLabel" for="RepairSkillID" >{$page['Labels']['repair_skill']|escape:'html'}:<sup>*</sup></label>

                                &nbsp;&nbsp;  
                                <select name="RepairSkillID" id="RepairSkillID" class="text" >
                                <option value="" {if $datarow.RepairSkillID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $repairSkills as $repairSkill}

                                    <option value="{$repairSkill.RepairSkillID}" {if $datarow.RepairSkillID eq $repairSkill.RepairSkillID}selected="selected"{/if}>{$repairSkill.RepairSkillName|escape:'html'}</option>

                                {/foreach}

                                </select>
                         </p>
                        
                        
                        <p>
                                <label class="fieldLabel" for="ManufacturerID" >{$page['Labels']['manufacturer']|escape:'html'}:<sup>*</sup></label>

                                &nbsp;&nbsp;  
                                <select name="ManufacturerID" id="ManufacturerID" class="text" >
                                <option value="" {if $datarow.ManufacturerID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $manufacturers as $manufacturer}

                                    <option value="{$manufacturer.ManufacturerID}" {if $datarow.ManufacturerID eq $manufacturer.ManufacturerID}selected="selected"{/if}>{$manufacturer.ManufacturerName|escape:'html'}</option>

                                {/foreach}

                                </select>
                         </p>
                        
                        
                         <p>
                                <label class="fieldLabel" for="JobTypeID" >{$page['Labels']['job_type']|escape:'html'}:</label>

                                &nbsp;&nbsp;  
                                <select name="JobTypeID" id="JobTypeID" class="text" >
                                <option value="" {if $datarow.JobTypeID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $jobTypes as $jobType}

                                    <option value="{$jobType.JobTypeID}" {if $datarow.JobTypeID eq $jobType.JobTypeID}selected="selected"{/if}>{$jobType.Type|escape:'html'}</option>

                                {/foreach}

                                </select>
                         </p>
                         
                         <p>
                                <label class="fieldLabel" for="ClientID" >{$page['Labels']['client']|escape:'html'}:</label>

                                &nbsp;&nbsp;  
                                <select name="ClientID" id="ClientID" class="text" >
                                <option value="" {if $datarow.ClientID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $clients as $client}

                                    <option value="{$client.ClientID}" {if $datarow.ClientID eq $client.ClientID}selected="selected"{/if}>{$client.ClientName|escape:'html'}</option>

                                {/foreach}

                                </select>
                         </p>
                         
                         
                         <p>
                                <label class="fieldLabel" for="ServiceTypeID" >{$page['Labels']['service_type']|escape:'html'}:</label>

                                &nbsp;&nbsp;  
                                <select name="ServiceTypeID" id="ServiceTypeID" class="text" >
                                <option value="" {if $datarow.ServiceTypeID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $serviceTypes as $serviceType}

                                    <option value="{$serviceType.ServiceTypeID}" {if $datarow.ServiceTypeID eq $serviceType.ServiceTypeID}selected="selected"{/if}>{$serviceType.Name|escape:'html'}</option>

                                {/foreach}

                                </select>
                        </p>
                         
                             
                        <p>
                            <label class="fieldLabel" for="Status" >{$page['Labels']['out_of_area']|escape:'html'}:</label>

                            &nbsp;&nbsp; 

                             <input  type="checkbox" name="OutOfArea"  value="1" {if $datarow.OutOfArea eq 'Yes'} checked="checked" {/if}  /> 

                                 
                        </p>
                        
                        
                         <p>
                            <label class="fieldLabel" for="PostcodeFiles" >{$page['Labels']['files_to_import']|escape:'html'}:</label>
                           
                             
                             
                             <span id="html4_uploader" class="postCodesBox" >You browser doesn't have HTML 4 support.</span>
                             
                             
                             
                             <input  type="hidden" class="text"   name="fileUploadPrefix" value="{$fileUploadPrefix|escape:'html'}"  id="fileUploadPrefix" >
                             <!--
                             <input  type="file" class="text multi"  style="width:225px"   name="PostcodeFiles[]"  id="PostcodeFiles" >
                            -->
                         </p>
                        
                            
                         {/if}

                        <p>

                            <span class= "bottomButtons" >

                                <input type="hidden" name="PostcodeAllocationID"  value="{$datarow.PostcodeAllocationID|escape:'html'}" >


                                {if $datarow.PostcodeAllocationID neq '' && $datarow.PostcodeAllocationID neq '0'}

                                {else}
                                 
                                    
                                    <span class="centerBtn" id="display_process_text_id" ></span>
                                    
                                    <input type="submit" name="insert_save_btn" class="textSubmitButton centerBtn" id="insert_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >

                                {/if}

                                    <br>
                                    <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >



                            </span>

                        </p>

                           
                </fieldset>    
                        
                </form>        
                                             
       
</div>
                 
{/if}  
                                    
                                    
{* This block of code is for to display message after data updation *} 

                                    
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>    

                            
                        
