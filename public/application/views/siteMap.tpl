{extends "DemoLayout.tpl"}


{block name=config}
    {$PageId = $SiteMapPage}
{/block}


{block name=afterJqueryUI}
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" media="screen" charset="utf-8" />
    
    <script src="{$_subdomain}/js/jquery.multiselect.js" type="text/javascript"></script>
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
    <link href="{$_subdomain}/css/themes/pccs/jquery.multiselect.css" rel="stylesheet" type="text/css" />
{/block}


{block name=scripts}
    
<script type="text/javascript">


    $(document).ready(function() {
    
	$height1 = $('#firstDiv').height();
	$height2 = $('#secondDiv').height();
	//$height3 = $('#thirdDiv').height();
	$maxHeight = $height1;
	if($height2 > $maxHeight) {
	   $maxHeight = $height2;
	} 
	
	//if($height3 > $maxHeight) {
	//   $maxHeight = $height3;
	//}
     
	$('#firstDiv').css("height", $maxHeight+"px");
	$('#secondDiv').css("height", $maxHeight+"px");
	//$('#thirdDiv').css("height", $maxHeight+"px");
   
	/*
	$("body").on('DOMNodeInserted', "select", function () {
	    alert('select loaded');
	});
	*/

	function networkChange() {
 	    
	    $("#client").next(".ui-combobox").remove();
	    $("#branch").next(".ui-multiselect").remove();
	    $("#branch, #client").remove();
	    $("label[for=branch], label[for=client]").remove();
		
	    if($("#network").val() != "") {
	    
		$.post("{$_subdomain}/Data/getNetworkClients", { networkID: $("#network").val() }, function(response) {
		    var data = $.parseJSON(response);
		    var html = "<label for='client'>Client: </label>\
				<select id='client' name='client'>\
				    <option value=''>Select Client</option>\
			       ";
		    for (var i = 0; i < data.length; i++) {
			html += "<option value='" + data[i]["ClientID"] + "'>" + data[i]["ClientName"] + "</option>";
		    }
		    html += "</select>";
		    $(html).insertAfter($("#network").next(".ui-combobox"));
		    
		    $("#client").combobox({
			change: function() { clientChange($(this)); }
		    });
		    
		    $.colorbox.resize();
		});
		
	    }
						
	}


	function clientChange() {
	
	    $("#branch").next(".ui-multiselect").remove();
	    $("#branch").remove();
	    $("label[for=branch]").remove();
		
	    if($("#client").val() != "") {
	    
		$.post("{$_subdomain}/Data/getClientBranches", { clientID: $("#client").val() }, function(response) {
		    var data = $.parseJSON(response);
		    var html = "<label for='branch'>Branch: </label>\
				<select id='branch' name='branch'>\
			       ";
		    for (var i = 0; i < data.length; i++) {
			html += "<option value='" + data[i]["BranchID"] + "'>" + data[i]["BranchName"] + "</option>";
		    }
		    html += "</select>";
		    $(html).insertAfter($("#client").next(".ui-combobox"));
		    
		    $("#branch").multiselect({ noneSelectedText: "" });
		    $("#branch").multiselect("uncheckAll");
		    
		    $.colorbox.resize();
		});
		
	    }
						
	}


	$(document).on("click", "#samsungTransactionReport", function() {
	    
	    $.post("{$_subdomain}/index/getSamsungTransactionReportModal", { }, function(response) {
		$.colorbox({
		    html :	response, 
		    width:	"520px",
		    scrolling:  false,
		    onComplete: function() {
	    
			$("input[name=dateFrom], input[name=dateTo]").datepicker({ 
			    dateFormat:	"dd/mm/yy",
			    changeMonth:true,
			    changeYear: true				    
			});
			
			$("#network").combobox({ 
			    change: function() { networkChange(); } 
			});
			
			$("#client").combobox({
			    change: function() { clientChange(); }
			});
			
			$("#branch").multiselect({ noneSelectedText: "" });
			$("#branch").multiselect("uncheckAll");
			
		    },
		    onClosed: function() {
			$("#generate").die();
		    },
		    escKey:	false,
		    overlayClose: false
		});
	    });
    
	    return false;
	    
	});
	
	
	$(document).on("click", "#serviceActionTrackingReport", function() {
	    
	    $.post("{$_subdomain}/index/getServiceActionTrackingReportModal", { }, function(response) {
		$.colorbox({
		    html :	response, 
		    width:	"520px",
		    scrolling:  false,
		    onComplete: function() {
	    
			$("input[name=dateFrom], input[name=dateTo]").datepicker({ 
			    dateFormat:	"yy-mm-dd",
			    changeMonth:true,
			    changeYear: true				    
			});
			
			$("#network").combobox({ 
			    change: function() { networkChange(); } 
			});
			
			$("#client").combobox({
			    change: function() { clientChange(); }
			});
			
			$("#branch").multiselect({ noneSelectedText: "" });
			$("#branch").multiselect("uncheckAll");
			
		    },
		    onClosed: function() {
			$("#generate").die();
		    },
		    escKey:	false,
		    overlayClose: false
		});
	    });
    
	    return false;
	    
	});
	
    
    });


$(document).on("click", "#stockReceivingA", function() {


		$.colorbox({
		    href :	"{$_subdomain}/Stock/stockReiceivingWindow",
		    scrolling:  false,
		    onComplete: function() {
	    
			$("input[name=dateFrom], input[name=dateTo]").datepicker({ 
			    dateFormat:	'dd/mm/yy',
			    changeMonth:true,
			    changeYear: true				    
			});
			
			$("#network").combobox({ 
			    change: function() { networkChange(); } 
			});
			
			$("#client").combobox({
			    change: function() { clientChange(); }
			});
			
			$("#branch").multiselect();
			
		    },
		    onClosed: function() {
			$("#generate").die();
		    },
		    escKey:	false,
		    overlayClose: false
		});
	   
    
	    
	    
	});
        
        
$(document).on("click", "#minimumStockRoutineA", function() {


		$.colorbox({
		    href :	"{$_subdomain}/Stock/minimumStockRoutine",
		    scrolling:  false,
		    onComplete: function() {
	    
			
		    },
		    onClosed: function() {
			
		    },
		    escKey:	false,
		    overlayClose: false
		});
	   
    
	    
	    
	});
$(document).on("click", "#stockUsageReportA", function() {


		$.colorbox({
		    href :	"{$_subdomain}/Stock/stockUsageReport",
		    scrolling:  false,
		    onComplete: function() {
	    
			
		    },
		    onClosed: function() {
			
		    },
		    escKey:	false,
		    overlayClose: false
		});
	   
    
	    
	    
	});
	
    
    
</script>


{/block}


{block name=body}

    
<div class="breadcrumb">
    <div>
        <a href="{$_subdomain}/index/index">{$page['Text']['home_page']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}
    </div>
</div>
    
{include file='include/menu.tpl'}

        
<div class="main" id="siteMap">   
   
   <div class="siteMapPanel" >
         <form id="siteMapForm" name="siteMapForm" method="post"  action="#" class="inline">
       
             
            <fieldset>
            <legend title="" >{$page['Text']['page_title']|escape:'html'}</legend>
             
                        <div id="firstDiv" style="width:32%;float:left;" >
                           
                           
                           {if $AP11006}
                            <p>
                            <h3>{$page['Text']['appointment_diary']|escape:'html'}</h3>
                            <a href="{$_subdomain}/Diary/default" >{$page['Text']['diary_main']|escape:'html'}</a> {$page['Text']['diary_main_text']|escape:'html'}
                            <br>
                            <br>
                            
                            <a href="{$_subdomain}/AppointmentDiary/" >{$page['Text']['diary_defaults']|escape:'html'}</a> {$page['Text']['diary_defaults_text']|escape:'html'}
                           
                            
                            <br>
                            <br>
                             <a href="{$_subdomain}/Diary/wallboard" >{$page['Text']['diary_wallboard']|escape:'html'}</a> {$page['Text']['diary_wallboard_text']|escape:'html'}
                            </p>
                            
                            {/if}
                            
                          
                            <p>
                            <h3>Stock Control</h3>
                            <a href="{$_subdomain}/Stock/default" >Stock Control Table</a> This link will display a list of all parts held within the system
                            <br>
                             <br>
                           
                             <a href="#" id="minimumStockRoutineA">Minimum Stock Routine</a> This link will generate a report detailing stock that has reached minimum
                             order levels
                            <br>
                              <br>
                             
                             
                              
                            <a href="#" id="stockReceivingA">Stock Receiving</a> This link will display a page that will allow the user to manually receive stock and update quantities. 
                            <br>
                              <br>
                              <a href="#" id="stockUsageReportA">Stock Usage Report</a> This report will list all stock used within a specified date range.
                            stock and update quantities.
                            <br>
                              
                             
                           </p>
                           
                            {if $loggedin_user->Username == "sa" || $AP8001|default:"false"==true}
                         
                            <p>
                            <h3>Stock Control (NEW)</h3>
                            <a href="{$_subdomain}/StockControl/default" >Stock Control Table</a> This link will display a list of all parts held within the system
                            <br>
                             <br>
                              <a href="{$_subdomain}/StockControl/stockOrdering" >Pending Orders</a> This screen will display parts added to a job. Part orders and requisitions can be created from here
                            <br>
                              <br>
                              <a href="{$_subdomain}/StockControl/stockReceivingOrders" >Receive Stock Orders</a> This screen will display all stock orders and allow orders to be received
                            <br>
                              <br>
                            
                              
                             
                           </p>
                           
                           {/if}
                           
                           
                           
                           
                           
                          
                       
                            
                            {if $AP11002}
                                <p>
                                    <h3>{$page['Text']['reports']|escape:'html'}</h3>

                                    <a href="#">{$page['Text']['csv_job_export']|escape:'html'}</a> {$page['Text']['csv_job_export_text']|escape:'html'}

                                </p>
                            {/if}
                            

			    {*  Requested by Joe 
                            <p>
				<h3>Appointment Allocation</h3>
				<a href="{$_subdomain}/Diary/default" >Global</a> 
				View Diary System and allocate available slots. 
                            </p>
			    

                            <p>
				<h3>Appointment Allocation</h3>
				<a href="{$_subdomain}/diary/Diary/default" >Service Provider</a> 
				View Diary System and allocate available slots. 
                            </p>
                            
                            *}
			    
			    
			    {*REPORT GENERATOR***********************************************}
			    
			    {if $loggedin_user->Username == "sa" || $loggedin_user->Username == "jbcam"}
				<p>
				    <h3>{$page['Text']['report_generator']|escape:'html'}</h3>
				    <a href="{$_subdomain}/Report/generator" id="reportGenerator">{$page['Text']['report_generator_link']|escape:'html'}</a> {$page['Text']['report_generator_text']|escape:'html'}
				</p>
			    {/if}
			    
			    {****************************************************************}
			    
			    
                            {if $AP11003}
				<p>
				    <h3>{$page['Text']['reports_samsung']|escape:'html'}</h3>
				    <a href="#" id="samsungTransactionReport">Transaction Report</a><br/> {*$page['Text']['astr_report_text']|escape:'html'*}
				    <a href="#" id="serviceActionTrackingReport">Service Action Tracking Report</a> {*$page['Text']['astr_report_text']|escape:'html'*}
				</p>
                            {/if}
                            
			    
                            {if $AP11004}
                            <p>
                            <h3>{$page['Text']['request_authorisation']|escape:'html'}</h3>
                            
                            <a href="{$_subdomain}/Job/raJobs" >{$page['Text']['ra_processing']|escape:'html'}</a> {$page['Text']['ra_processing_text']|escape:'html'}

                            </p>
                            {/if}
                            
                            
                            {if $displayWallBoardLink}
                            
                            {if $AP11007}
                                <p>    

                                    <h3>{$page['Text']['booking_performance_wallboard']|escape:'html'}</h3>

                                    <a href="{$_subdomain}/Job/bookingPerformance" >{$page['Text']['booking_performance']|escape:'html'}</a> {$page['Text']['booking_performance_text']|escape:'html'}

                                </p>
                            {/if}
                            
                            {if $AP11008}
                                <p>    
                                    <h3>{$page['Text']['diary_wall_board']|escape:'html'}</h3>
                                    <a href="{$_subdomain}/AppointmentDiary/wallboard" >{$page['Text']['diary_wall_board']|escape:'html'}</a> {$page['Text']['diary_wall_board_text']|escape:'html'}

                                </p>
                            {/if}
                            
                            
                            {/if}
                            
                             <!--system admin-->
                              {if $loggedin_user->Username == "sa" || $AP8001|default:"false"==true}
                           <p>
                           
                           <h3>System Admin</h3>
                            
                            <a href="{$_subdomain}/SystemAdmin" >System Admin</a> This link will display a list of all system defaults within the system
                            <br>
                           
                           
                            
                              <br>
                              </p>
                              {/if}
                        </div>
                        
                        <div id="secondDiv" style="width:64%;float:left;margin-left:15px;margin-right:10px;"" >
                            <div id="rightContent" style="margin-left: 150px">
                          <h3>Consumer Feedback</h3> 
 <a href="{$_subdomain}/LookupTables/satisfactionQuestionnaire/report" > Customer Satisfaction Questionnaire Report </a><br>
Generate a report to view your customer satisfaction <br />questionnaire results

                            <h2 style="padding-top:150px;padding-left:100px;" ></h2>
                        </div>
                        </div>
                       <!-- <div id="thirdDiv" style="width:32%;float:right;" >
                            
                        </div>-->
          
             </fieldset> 

        
        </form>
    </div>  
                                    
</div>
{/block}
