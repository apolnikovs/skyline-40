{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $Country}
{/block}

    {block name=afterJqueryUI}
        <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script> 
        <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
        <style type="text/css" >
        .ui-combobox-input {
            width:300px;
        }
        </style>
    {/block}
{block name=scripts}


    {*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}


    <script type="text/javascript">
        
    var $statuses = [
			{foreach from=$statuses item=st}
			   ["{$st.Name}", "{$st.Code}"],
			{/foreach}
                    ]; 


    $(document).on("click", "#deleteButtonID", function() {

	var id=$(".row_selected").find("td:first-child").text();
	
	var html='  \
		    <div id="deleteDiv">\
			<fieldset>\
			    <legend>Delete Record</legend>\
			    <p id="deleteP">Are you sure you want to delete row with id <strong>' + id + '</strong>?</p>\
			    <div id="deleteButtonWrapper">\
				<button type="button" id="deleteYes">Yes</a>\
				<button type="button" id="deleteNo">No</a>\
				<button type="button" id="deleteClose" style="display:none;">Close</a>\
			    </div>\
			</fieldset>\
		    </div>\
		';
	
	$.colorbox({ html : html, title : "Delete Record", scrolling : false });
	return false;
    });

    $(document).on("click", "#deleteYes", function() {
	var id=$(".row_selected").find("td:first-child").text();
	$.post("{$_subdomain}/SystemAdmin/deleteAltField/", { id : id}, function(data) {
	    $("#deleteP").text("Record was successfuly deleted.");
	    $("#deleteYes, #deleteNo").css("display","none");
	    $("#deleteClose").css("display","inline");
	});
    });

    $(document).on("click", "#deleteNo", function() {
	$.colorbox.close();
	return false;
    });

    $(document).on("click", "#deleteClose", function() {
	location.reload();
	return false;
    });


    function gotoEditPage($sRow) {
        $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
        $('#updateButtonId').trigger('click');
    }

    function inactiveRow(nRow, aData) {
          
        if (aData[3]==$statuses[1][1]) {  
            $(nRow).addClass("inactive");
            $('td:eq(3)', nRow).html( $statuses[1][0] );
        } else {
            $(nRow).addClass("");
            $('td:eq(3)', nRow).html( $statuses[0][0] );
        }
	
    }
               
    function gotoInsertPage($sRow) {
       
        $("#copyRowId").val($sRow[0]);
        $('#addButtonId').trigger('click');
        $("#copyRowId").val('');
        
    }

    $(document).ready(function() {

	//Click handler for finish button.
	$(document).on('click', '#finish_btn', function() {
	    
	    $(location).attr('href', '{$_subdomain}/SystemAdmin/index/lookupTables');
	    
	});


	/* =======================================================
	 *
	 * set tab on return for input elements with form submit on auto-submit class...
	 *
	 * ======================================================= */

	$('input[type=text],input[type=password]').keypress( function( e ) {
	    
	    if (e.which == 13) {
		$(this).blur();
		if($(this).hasClass('auto-submit')) {
		    $('.auto-hint').each(function() {
			$this = $(this);
			if($this.val() == $this.attr('title')) {
			    $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
			    if ($this.hasClass('auto-pwd')) {
				$this.prop('type','password');
			    }
			}
		    });
		    
		    $(this).get(0).form.onsubmit();
		    
		} else {
		    $next = $(this).attr('tabIndex') + 1;
		    $('[tabIndex="'+$next+'"]').focus();
		}
		
		return false;
		      
	    }
	    
	});        


	var displayButtons = "UDA";

	$('#CResults').PCCSDataTable( {

	    aoColumns: [ 
		/* Field ID		*/  { sWidth: "10%" },  
		/* Primary field name	*/  { sWidth: "25%" }, 
		/* Alt field name	*/  { sWidth: "25%" }, 
		/* Status		*/  { sWidth: "10%" }, 
		/* Brand		*/  { sWidth: "20%" },
		/* Brand ID		*/  { sWidth: "10%" }, 
	    ],

	    displayButtons:	displayButtons,
	    addButtonId:	'addButtonId',
	    addButtonText:	'{$page['Buttons']['insert']|escape:'html'}',
	    createFormTitle:	'{$page['Text']['insert_page_legend']|escape:'html'}',
	    createAppUrl:	'{$_subdomain}/SystemAdmin/alternativeFieldNames/insert/',
	    createDataUrl:	'{$_subdomain}/LookupTables/ProcessData/AlternativeFieldNames/',
	    formInsertButton:	'insert_save_btn',
	    copyInsertRowId:	'copyRowId',
	    
	    deleteButtonId:	"deleteButtonID",
	    //deleteAppUrl:	'{$_subdomain}/SystemAdmin/alternativeFieldNames/deleteForm/',
	    //deleteDataUrl:	'{$_subdomain}/SystemAdmin/alternativeFieldNames/delete/',

	    frmErrorRules: {
		alternativeFieldName:	{ required: true },
		brandID:		{ required: true },
		primaryFieldID:		{ required: true }
	    },

	    frmErrorMessages: {
		alternativeFieldName:   { required: "{$page['Text']['alternative_field_name_error']|escape:'html'}" },
		brandID:		{ required: "{$page['Text']['brand_id_error']|escape:'html'}" },
		primaryFieldID:		{ required: "{$page['Text']['primary_field_error']|escape:'html'}" }
	    },                     

	    popUpFormWidth:	715,
	    popUpFormHeight:	430,
	    pickButtonId:	"copy_btn",
	    pickButtonText:	"{$page['Buttons']['copy']|escape:'html'}",
	    pickCallbackMethod: "gotoInsertPage",
	    colorboxForceClose: false,
	    updateButtonId:	'updateButtonId',
	    updateButtonText:	'{$page['Buttons']['edit']|escape:'html'}',
	    updateFormTitle:	'{$page['Text']['update_page_legend']|escape:'html'}',
	    updateAppUrl:	'{$_subdomain}/SystemAdmin/alternativeFieldNames/update/',
	    updateDataUrl:	'{$_subdomain}/LookupTables/ProcessData/AlternativeFieldNames/',
	    formUpdateButton:	'update_save_btn',
	    colorboxFormId:	"CForm",
	    frmErrorMsgClass:	"fieldError",
	    frmErrorElement:	"label",
	    htmlTablePageId:	'CResultsPanel',
	    htmlTableId:	'CResults',
	    fetchDataUrl:	'{$_subdomain}/LookupTables/ProcessData/AlternativeFieldNames/fetch/',
	    formCancelButton:	'cancel_btn',
	    dblclickCallbackMethod: 'gotoEditPage',
	    fnRowCallback:      'inactiveRow',
	    searchCloseImage:	'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
	    tooltipTitle:	"{$page['Text']['tooltip_title']|escape:'html'}",
	    iDisplayLength:	25,
	    formDataErrorMsgId: "suggestText",
	    frmErrorSugMsgClass:"formCommonError"

	});
         
	 
    });

    </script>
    
{/block}


{block name=body}

    <div class="breadcrumb">
        <div>
	    <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / {$page['Text']['alternative_field_names']|escape:'html'}
        </div>
    </div>


    <div class="main" id="home">

	<div class="LTTopPanel" >
	    <form id="countryForm" name="countryForm" method="post"  action="#" class="inline">
		<fieldset>
		    <legend title="" >{$page['Text']['legend']|escape:'html'}</legend>
		    <p><label>{$page['Text']['description']|escape:'html'}</label></p> 
		</fieldset> 
	    </form>
	</div>  


	<div class="LTResultsPanel" id="CResultsPanel">

	    <table id="CResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
		<thead>
		    <tr>
			<th title="{$page['Text']['field_id']|escape:'html'}" >{$page['Text']['field_id']|escape:'html'}</th>
			<th title="{$page['Text']['primary_name']|escape:'html'}" >{$page['Text']['primary_name']|escape:'html'}</th>
			<th title="{$page['Text']['alternative_name']|escape:'html'}"  >{$page['Text']['alternative_name']|escape:'html'}</th>
			<th title="{$page['Text']['status']|escape:'html'}" >{$page['Text']['status']|escape:'html'}</th>
			<th title="{$page['Text']['brand']|escape:'html'}" >{$page['Text']['brand']|escape:'html'}</th>
			<th title="{$page['Text']['brand_id']|escape:'html'}" >{$page['Text']['brand_id']|escape:'html'}</th>
		    </tr>
		</thead>
		<tbody>
		</tbody>
	    </table>  
		    
	</div>        

	<div class="bottomButtonsPanel" >
	    <hr>
	    <button id="finish_btn" type="button" class="gplus-blue rightBtn" ><span class="label">{$page['Buttons']['finish']|escape:'html'}</span></button>
	</div>        

	<input type="hidden" name="copyRowId" id="copyRowId" > 

	{if $SupderAdmin eq false} 

	   <input type="hidden" name="addButtonId" id="addButtonId" > 

	{/if} 
 
    </div>


{/block}



