<?php
require_once('TableSQL.class.php');

/**
 * TableFactory Class 
 *
 * This Factory class returns instances of the TableSQL class for all
 * tables in the skyline database.
 * 
 * Example:
 * 
 * <code>
 * // From within a model file include the factory class file...
 * require_once('TableFactory.class.php');
 * 
 * // establish a d/b connection...
 * $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                 $this->controller->config['DataBase']['Username'],
                                 $this->controller->config['DataBase']['Password'] );
 * 
 * // create an instance of the TableSQL class...
 * $table = TableFactory::myTable();
 * 
 * // create an array of columns and values...
 * $params = array (
 *              'desc' => 'some descriptive text for new row.',
 *              'value' => 100.45,
 *              'count' => 42,
 *              'actionCode' => 14
 *           );
 * 
 * // generate an insert command and add row to table...
 * $cmd = $table->insertCommand( $params );
 * $this->Execute($this->conn, $cmd, $params);
 * 
 * // create an array of columns and values...
 * $params = array (
 *              'id' => 101,  // Note: Primary Key
 *              'desc' => 'some descriptive text for row 101',
 *              'value' => 97.50,
 *              'count' => 137,
 *              'actionCode' => 12
 *           );
 * 
 * // generate an update command and update row in table... 
 * $cmd = $table->updateCommand( $params ); 
 * $this->Execute($this->conn, $cmd, $params);
 * 
 *  * // create an array of columns and values...
 * $params = array (
 *              'id' => 101,  // Note: Primary Key
 *           );
 * 
 * // generate a delete command and delete row from table...
 * $cmd = $table->deleteCommand() ;
 * $this->Execute($this->conn, $cmd, $params);
 * 
 * // generate a delete command with where condition and delete row from table...
 * $cmd = $table->deleteCommand( 'actionCode=12 and count>30' ) ;
 * $this->Execute($this->conn, $cmd );
 * </code>
 * 
 * @author      Brian Etherington
 * @version     1.0
 */

class TableFactory {  
    
    //public static function MyTable() { return new TableSQL('myTable','id'); }  
    //public static function MyTable2() { return new TableSQL('myTable2','id_1,id_2',false); }
    
    public static function Network() { return new TableSQL('network','NetworkID'); }
    public static function Manufacturer() { return new TableSQL('manufacturer','ManufacturerID'); }
    public static function Model() { return new TableSQL('model','ModelID'); }
    public static function ServiceType() { return new TableSQL('service_type','ServiceTypeID'); }
    public static function ServiceTypeAlias() { return new TableSQL('service_type_alias','ServiceTypeAliasID'); }
    public static function RepairSkill() { return new TableSQL('repair_skill','RepairSkillID'); }
    public static function Client() { return new TableSQL('client','ClientID'); }
    public static function ClientPurchaseOrder() { return new TableSQL('client_purchase+order','ClientPurchaseOrderID'); }
    public static function ServiceProvider() { return new TableSQL('service_provider','ServiceProviderID'); }
    public static function UnitType() { return new TableSQL('unit_type','UnitTypeID'); }
    public static function Brand() { return new TableSQL('brand','BrandID',false); }
    public static function User() { return new TableSQL('user','UserID'); }
    public static function Branch() { return new TableSQL('branch','BranchID'); }
    public static function Product() { return new TableSQL('product','ProductID'); }
    public static function UnitPricingStructure() { return new TableSQL('unit_pricing_structure','UnitPricingStructureID'); }
    public static function SecurityQuestion() { return new TableSQL('security_question','SecurityQuestionID'); }
    public static function Role() { return new TableSQL('role','RoleID'); }
    public static function Permission() { return new TableSQL('permission','PermissionID'); }
    public static function CustomerTitle() { return new TableSQL('customer_title','CustomerTitleID'); }
    public static function Customer() { return new TableSQL('customer','CustomerID'); }
    public static function Status() { return new TableSQL('status','StatusID'); }
    public static function StatusHistory() { return new TableSQL('status_history','StatusHistoryID'); }
    public static function Job() { return new TableSQL('job','JobID'); }
    public static function Part() {return new TableSQL('part','PartID'); }
    public static function Appointment() { return new TableSQL('appointment','AppointmentID'); }
    public static function ProductLocation() { return new TableSQL('product_location','ProductLocationID'); }
    public static function JobType() { return new TableSQL('job_type','JobTypeID'); }
    public static function Audit() { return new TableSQL('audit','AuditID'); }
    public static function AuditTrailAction() { return new TableSQL('audit_trail_action','AuditTrailActionID'); }
    public static function ContactHistory() { return new TableSQL('contact_history','ContactHistoryID'); }
    public static function ContactHistoryAction() { return new TableSQL('contact_history_action','ContactHistoryActionID'); }
    public static function County() { return new TableSQL('county','CountyID'); }
    public static function Country() { return new TableSQL('country','CountryID'); }
    public static function PaymentType() { return new TableSQL('payment_type','PaymentTypeID'); }
    public static function VatRate() { return new TableSQL('vat_rate','VatRateID'); }
    public static function GeneralDefault() { return new TableSQL('general_default','GeneralDefaultID'); }
    public static function CompletionStatus() { return new TableSQL('completion_status','CompletionStatusID'); }
    public static function CentralServiceAllocation() { return new TableSQL('central_service_allocation','CentralServiceAllocationID'); }
    public static function PostcodeAllocation() { return new TableSQL('postcode_allocation','PostcodeAllocationID'); }
    public static function TownAllocation() { return new TableSQL('town_allocation','TownAllocationID'); }
    public static function UserRole() { return new TableSQL('user_role','UserID,RoleID',false); }
    public static function AssignedPermission() { return new TableSQL('assigned_permission','PermissionID,RoleID',false); }
    public static function ClaimResponse() { return new TableSQL('claim_response','SamsungClaimResponseID'); }
    public static function Email() { return new TableSQL('email','EmailID'); }
    public static function EmailJob() { return new TableSQL('email_job','EmailJobID'); }
    public static function DiaryAllocation() { return new TableSQL('diary_allocation','DiaryAllocationID'); }
    public static function PostcodeLatLong() { return new TableSQL('postcode_lat_long','PostCode'); }
    public static function DiarySPMap() { return new TableSQL('diary_sp_map', 'diarySPMapID'); }
    public static function Skillset() { return new TableSQL('skillset', 'SkillsetID'); }
    public static function ServiceProviderEngineer() { return new TableSQL('service_provider_engineer', 'ServiceProviderEngineerID'); }
    public static function NonSkylineJob() { return new TableSQL('non_skyline_job', 'NonSkylineJobID'); }
    public static function ManufacturerServiceProvider() { return new TableSQL('manufacturer_service_provider','ManufacturerID,ServiceProviderID',false); }
    public static function JobSource() { return new TableSQL('job_source', 'JobSourceID'); }
    public static function UnitTypeManufacturer() { return new TableSQL('unit_type_manufacturer', 'UnitTypeManufacturerID'); }
    public static function AppointmentType() { return new TableSQL('appointment_type', 'AppointmentTypeID'); }
    public static function AllocationReassignment() { return new TableSQL('allocation_reassignment', 'AllocationAssignmentID'); }
    public static function StatusPermission() { return new TableSQL('status_permission', 'StatusPermissionID'); }
    public static function Shipping() { return new TableSQL('shipping', 'ShippingID'); }
    public static function Courier() { return new TableSQL('courier', 'CourierID'); }
    public static function Currency() { return new TableSQL('currency', 'CurrencyID'); }
    public static function CurrencyRate() { return new TableSQL('currency', 'CurrencyRateID'); }
    public static function Questionnaire() { return new TableSQL('questionnaire', 'QuestionnaireID'); }
    public static function QuestionnaireData() { return new TableSQL('questionnaire_data', 'QuestionnaireDataID'); }
    public static function QuestionnaireLog() { return new TableSQL('questionnaire_log', 'QuestionnaireLog',false); }
    public static function QuestionnaireAttribute() { return new TableSQL('questionnaire_attribute', 'QuestionnaireAttributeID'); }
    public static function Recon() { return new TableSQL('recon', 'ReconID'); }
    public static function DiaryMatrix() { return new TableSQL('diary_matrix', 'DiaryMatrixID'); }
    public static function PackagingType() { return new TableSQL('packaging_type', 'PackagingTypeID'); }
    public static function OneTouchAudit() { return new TableSQL('one_touch_audit', 'OneTouchAuditID'); }
    public static function JobSettings() { return new TableSQL('job_settings', 'JobSettingsID'); }
    public static function gaugePreferences() { return new TableSQL('gauge_preferences', 'GaugeID'); }
    public static function OpenJobsStatusPermission() { return new TableSQL('open_jobs_status_permission', 'OpenJobsStatusPermissionID'); }
}
?>
