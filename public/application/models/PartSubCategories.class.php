<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of PartSubCategory Page in Lookup Tables section under System Admin
 *
 * @author      Andris Polnikovs <a.polnikovs@gmail.com>
 * @version     1.0 
 * @created     04/06/2013
 */
class PartSubCategories extends CustomModel {

    public function __construct($controller) {

        parent::__construct($controller);

        $this->conn = $this->Connect($this->controller->config['DataBase']['Conn'], $this->controller->config['DataBase']['Username'], $this->controller->config['DataBase']['Password']);
        $this->SQLGen = $this->controller->loadModel('SQLGenerator');
        $this->fields = [
            
            "SubCategoryName",
            "ServiceProviderPartCategoryID",
            "Status",
            "ServiceProviderID"
            
           
            
        ];
    }

    public function insertPartSubCategory($P, $spid) {
        $P["ServiceProviderID"] = $spid;
        $P["Status"] = isset($P["Status"]) ? $P["Status"] : "Active";
        $id = $this->SQLGen->dbInsert('service_provider_part_sub_category', $this->fields, $P, true, true);
        return $id;
    }

    public function updatePartSubCategory($P, $spid) {
        $P["ServiceProviderID"] = $spid;
        $P["Status"] = isset($P["Status"]) ? $P["Status"] : "Active";
        $id = $this->SQLGen->dbUpdate('service_provider_part_sub_category', $this->fields, $P, "ServiceProviderPartSubCategoryID=" . $P['ServiceProviderPartSubCategoryID'], true);
    }

    public function getPartSubCategoryData($id) {
        $sql = "select * from service_provider_part_sub_category where ServiceProviderPartSubCategoryID=$id";
        $res = $this->query($this->conn, $sql);
        return $res[0];
    }

    public function deletePartSubCategory($id) {
        $sql = "update service_provider_part_sub_category set Status='In-Active' where ServiceProviderPartSubCategoryID=$id";
        $this->execute($this->conn, $sql);
    }

   
    

}

?>