# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.251');

# ---------------------------------------------------------------------- #
# Modify Brand Table                                                     #
# ---------------------------------------------------------------------- # 
ALTER TABLE sms ADD BrandID INT( 11 ) NULL AFTER SMSBodyText;

# ---------------------------------------------------------------------- #
# Modify branch Table                                                    #
# ---------------------------------------------------------------------- # 
ALTER TABLE branch ADD SendRepairCompleteTextMessage ENUM( 'No', 'Yes' ) NULL DEFAULT 'No', ADD SMSID INT( 11 ) NULL ;


# ---------------------------------------------------------------------- #
# New Permissions(S)                                                     #
# ---------------------------------------------------------------------- # 
INSERT IGNORE INTO permission (PermissionID, Name, Description, ModifiedUserID, ModifiedDate) VALUES (12006, 'Closed Jobs - Display Graphical Dials', 'Closed Jobs - Display Graphical Dials', 1, '2013-05-03 13:36:29');

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.252');
