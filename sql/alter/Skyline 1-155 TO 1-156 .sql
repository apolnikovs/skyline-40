# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.155');


# ---------------------------------------------------------------------- #
# Modify table "service_provider_engineer"                                                     #
# ---------------------------------------------------------------------- #
ALTER TABLE `service_provider_engineer` ADD COLUMN `SpeedFactor` INT(11) NOT NULL DEFAULT '100' AFTER `Deleted`;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.156');




