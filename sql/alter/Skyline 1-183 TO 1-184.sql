# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.183');

# ---------------------------------------------------------------------- #
# Modify table "courier"                                                 #
# ---------------------------------------------------------------------- #
ALTER TABLE `courier` ADD COLUMN `CourierName` VARCHAR(100) NOT NULL AFTER `CourierID`;


# ---------------------------------------------------------------------- #
# Add table "shipping"                                                 #
# ---------------------------------------------------------------------- #

CREATE TABLE `shipping` 
( 
	`ShippingID` INT(11) NOT NULL AUTO_INCREMENT, 
	`JobID` INT(11) NOT NULL, 
	`CourierID` INT(11) NOT NULL, 
	`ConsignmentNo` VARCHAR(50) NULL DEFAULT NULL, 
	`ConsignmentDate` TIMESTAMP NULL DEFAULT NULL, 
	`ModifiedUserID` INT(11) NULL DEFAULT NULL, 
	`ModifiedDate` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
	PRIMARY KEY (`ShippingID`), 
	CONSTRAINT `job_TO_shipping` FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`), 
	CONSTRAINT `courier_TO_shipping` FOREIGN KEY (`CourierID`) REFERENCES `courier` (`CourierID`) 
) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.184');
