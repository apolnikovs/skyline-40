# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.233');


# ---------------------------------------------------------------------- #
# Modify table supplier                                                  #
# ---------------------------------------------------------------------- #
ALTER TABLE supplier ADD COLUMN ModifiedUserID INT NULL AFTER ApproveStatus, 
					 ADD COLUMN ModifiedDate TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER ModifiedUserID;
					 

# ---------------------------------------------------------------------- #
# Modify table service_provider_supplier                                 #
# ---------------------------------------------------------------------- #
ALTER TABLE service_provider_supplier CHANGE COLUMN ModifiedDate ModifiedDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER ModifiedUserID;


# ---------------------------------------------------------------------- #
# Add table service_provider_manufacturer                                #
# ---------------------------------------------------------------------- #
CREATE TABLE `service_provider_manufacturer` (
`ServiceProviderManufacturerID` INT(11) NOT NULL AUTO_INCREMENT,
`ManufacturerID` INT(11) NOT NULL,
`ManufacturerName` VARCHAR(30) NULL DEFAULT NULL,
`Acronym` VARCHAR(30) NULL DEFAULT NULL,
`CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
`EndDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
`Status` ENUM('Active','In-active') NOT NULL DEFAULT 'Active',
`BuildingNameNumber` VARCHAR(50) NULL DEFAULT NULL,
`Street` VARCHAR(100) NULL DEFAULT NULL,
`LocalArea` VARCHAR(100) NULL DEFAULT NULL,
`TownCity` VARCHAR(100) NULL DEFAULT NULL,
`CountryID` INT(3) NULL DEFAULT NULL,
`AuthorisationRequired` ENUM('Yes','No') NULL DEFAULT 'No',
`AuthReqChangedDate` TIMESTAMP NULL DEFAULT NULL,
`AuthorisationManager` VARCHAR(100) NULL DEFAULT NULL,
`AuthorisationManagerEmail` VARCHAR(100) NULL DEFAULT NULL,
`ManufacturerLogo` VARCHAR(40) NULL DEFAULT NULL,
`ModifiedUserID` INT(11) NULL DEFAULT NULL,
`ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
`AccountNo` VARCHAR(20) NULL DEFAULT NULL,
`AddressLine1` VARCHAR(60) NULL DEFAULT NULL,
`AddressLine2` VARCHAR(60) NULL DEFAULT NULL,
`AddressLine3` VARCHAR(60) NULL DEFAULT NULL,
`Postcode` VARCHAR(10) NULL DEFAULT NULL,
`TelNoSwitchboard` VARCHAR(20) NULL DEFAULT NULL,
`TelNoSpares` VARCHAR(20) NULL DEFAULT NULL,
`TelNoWarranty` VARCHAR(20) NULL DEFAULT NULL,
`TelNoTechnical` VARCHAR(20) NULL DEFAULT NULL,
`FaxNo` VARCHAR(20) NULL DEFAULT NULL,
`EmailAddress` VARCHAR(255) NULL DEFAULT NULL,
`WarrantyEmailAddress` VARCHAR(255) NULL DEFAULT NULL,
`SparesEmailAddress` VARCHAR(255) NULL DEFAULT NULL,
`ContactName` VARCHAR(30) NULL DEFAULT NULL,
`PrimaryServiceProviderSupplierID` INT(11) NULL DEFAULT NULL,
`StartClaimNo` VARCHAR(50) NULL DEFAULT NULL,
`VATNo` VARCHAR(20) NULL DEFAULT NULL,
`IRISManufacturer` ENUM('Yes','No') NULL DEFAULT NULL,
`FaultCodesReq` ENUM('Yes','No') NULL DEFAULT NULL,
`ExtendedGuaranteeNoReq` ENUM('Yes','No') NULL DEFAULT NULL,
`PolicyNoReq` ENUM('Yes','No') NULL DEFAULT NULL,
`InvoiceNoReq` ENUM('Yes','No') NULL DEFAULT NULL,
`CircuitRefReq` ENUM('Yes','No') NULL DEFAULT NULL,
`PartNoReq` ENUM('Yes','No') NULL DEFAULT NULL,
`OriginalRetailerReq` ENUM('Yes','No') NULL DEFAULT NULL,
`ReferralNoReq` ENUM('Yes','No') NULL DEFAULT NULL,
`SerialNoReq` ENUM('Yes','No') NULL DEFAULT NULL,
`FirmwareReq` ENUM('Yes','No') NULL DEFAULT NULL,
`WarrantyLabourRateReqOn` ENUM('Yes','No') NULL DEFAULT NULL,
`SageAccountNo` VARCHAR(20) NULL DEFAULT NULL,
`WarrantyLabourRateReq` ENUM('Yes','No') NULL DEFAULT NULL,
`NoWarrantyInvoices` ENUM('Yes','No') NULL DEFAULT NULL,
`DisplaySectionCodes` ENUM('Yes','No') NULL DEFAULT NULL,
`DisplayTwoConditionCodes` ENUM('Yes','No') NULL DEFAULT NULL,
`DisplayJobPriceStructure` ENUM('Yes','No') NULL DEFAULT NULL,
`DisplayTelNos` ENUM('Yes','No') NULL DEFAULT NULL,
`DisplayPartCost` ENUM('Yes','No') NULL DEFAULT NULL,
`DisplayRepairTime` ENUM('Yes','No') NULL DEFAULT NULL,
`DisplayAuthNo` ENUM('Yes','No') NULL DEFAULT NULL,
`DisplayOtherCosts` ENUM('Yes','No') NULL DEFAULT NULL,
`DisplayAdditionalFreight` ENUM('Yes','No') NULL DEFAULT NULL,
`NoOfClaimPrints` INT(11) NULL DEFAULT NULL,
`EDIClaimNo` INT(11) NULL DEFAULT NULL,
`EDIClaimFileName` VARCHAR(50) NULL DEFAULT NULL,
`UseProductCode` ENUM('Yes','No') NULL DEFAULT NULL,
`UseMSNNo` ENUM('Yes','No') NULL DEFAULT NULL,
`UseMSNFormat` ENUM('Yes','No') NULL DEFAULT NULL,
`MSNFormat` VARCHAR(20) NULL DEFAULT NULL,
`WarrantyPeriodFromDOP` INT(11) NULL DEFAULT NULL,
`ValidateDateCode` ENUM('Yes','No') NULL DEFAULT NULL,
`ForceStatusID` INT(11) NULL DEFAULT NULL,
`POPPeriod` INT(11) NULL DEFAULT NULL,
`ClaimPeriod` INT(11) NULL DEFAULT NULL,
`UseQA` ENUM('Yes','No') NULL DEFAULT NULL,
`UsePreQA` ENUM('Yes','No') NULL DEFAULT NULL,
`QAExchangeUnits` ENUM('Yes','No') NULL DEFAULT NULL,
`QALoanUnits` ENUM('Yes','No') NULL DEFAULT NULL,
`CompleteOnQAPass` ENUM('Yes','No') NULL DEFAULT NULL,
`ValidateNetworkQA` ENUM('Yes','No') NULL DEFAULT NULL,
`ExcludeFromBouncerTable` ENUM('Yes','No') NULL DEFAULT NULL,
`BouncerPeriod` INT(11) NULL DEFAULT NULL,
`BouncerPeriodBasedOn` ENUM('BookedDate','CompletedDate','DespatchedDate') NULL DEFAULT NULL,
`EDIExportFormatID` INT(11) NULL DEFAULT NULL,
`CreateTechnicalReport` ENUM('Yes','No') NULL DEFAULT NULL,
`CreateServiceHistoryReport` ENUM('Yes','No') NULL DEFAULT NULL,
`IncludeOutOfWarrantyJobs` ENUM('Yes','No') NULL DEFAULT NULL,
`WarrantyAccountNo` VARCHAR(20) NULL DEFAULT NULL,
`TechnicalReportIncludeAdj` ENUM('Yes','No') NULL DEFAULT NULL,
`PartNoReqForWarrantyAdj` ENUM('Yes','No') NULL DEFAULT NULL,
`ForceAdjOnNoParts` ENUM('Yes','No') NULL DEFAULT NULL,
`ForceKeyRepairPart` ENUM('Yes','No') NULL DEFAULT NULL,
`ForceOutOfWarrantyFaultCodes` ENUM('Yes','No') NULL DEFAULT NULL,
`WarrantyExchangeFree` DECIMAL(10,2) NULL DEFAULT NULL,
`UseFaultCodesForOBFJobs` ENUM('Yes','No') NULL DEFAULT NULL,
`AutoFinalRejectClaimsNotResubmitted` ENUM('Yes','No') NULL DEFAULT NULL,
`DaysToResubmit` INT(11) NULL DEFAULT NULL,
`LimitResubmissions` ENUM('Yes','No') NULL DEFAULT NULL,
`LimitResubmissionsTo` INT(11) NULL DEFAULT NULL,
`CopyDOPFromBouncerJob` ENUM('Yes','No') NULL DEFAULT NULL,
`ServiceProviderJobFaultCodeID` INT(11) NULL DEFAULT NULL,
`ServiceProviderPartFaultCodeID` INT(11) NULL DEFAULT NULL,
`ServiceProviderModelID` INT(11) NULL DEFAULT NULL,
`ServiceProviderID` INT(11) NULL DEFAULT NULL,
PRIMARY KEY (`ServiceProviderManufacturerID`),
INDEX `ManufacturerID` (`ManufacturerID`),
INDEX `ManufacturerName` (`ManufacturerName`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;



# ---------------------------------------------------------------------- #
# Modify table job                                                       #
# ---------------------------------------------------------------------- #
ALTER TABLE `job`
	ADD COLUMN `JobCategory` ENUM('service','installation','in_home_support') NULL DEFAULT NULL AFTER `ServiceCategory`,
	ADD COLUMN `CompletionStatusID` INT(11) NULL DEFAULT NULL AFTER `CompletionStatus`,
	ADD CONSTRAINT `completion_status_TO_job` FOREIGN KEY (`CompletionStatusID`) REFERENCES `completion_status` (`CompletionStatusID`);


# ---------------------------------------------------------------------- #
# Modify table completion_status                                         #
# ---------------------------------------------------------------------- #
ALTER TABLE `completion_status`
	ADD COLUMN `JobCategory` ENUM('service','installation','in_home_support') NULL DEFAULT NULL AFTER `CompletionStatusID`,
	ADD COLUMN `ServiceCategory` ENUM('soft','physical') NULL DEFAULT NULL AFTER `JobCategory`;

	
	
# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.234');
